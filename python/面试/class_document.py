# 面向对象-基本示例
class Test(object):
	def __init__(self, name, age):
		self.name = name
		self.age = age

	def print_xinxi(self):
		print('姓名：%s  年龄：%s'%(self.name, self.age))


# a=Test('zhangsn', '12')
# a.print_xinxi()


class Test1(object):
	def __init__(self):
		self.name = 'zhangsna'
		self.age = '12'

	def print_xinxi(self):
		print('姓名：%s  年龄：%s'%(self.name, self.age))


# a = Test1()
# a.print_xinxi()






# 访问限制
class Test2(object):
	def __init__(self):
		self.name = 'zhangsna'
		self.__age = '12'       # 变量名前加  __ ，限制外部访问

	def print_xinxi(self):
		print('姓名：%s  年龄：%s'%(self.name, self.__age))

# a = Test2()
# a.print_xinxi()
# print(a.name)
# print(a.__age) # 禁止外部访问，报错
# print(a._Test2__age)  # 可以访问






# 继承与多态
class TestJicheng1(object):
	def run(self):
		print('这是父类')

	def run2(self):
		print('这是父类的二号文件')

class TestJicheng2(TestJicheng1):
	def run(self):
		print('这是父类的大儿子')

	def duo_run(TestJicheng1):
		TestJicheng1.run2()
		TestJicheng1.run2()

class TestJicheng3(TestJicheng1):
	pass
		
# a=TestJicheng2()
# b=TestJicheng3()
# a.run()          # 这就是多态，子类的run()可以覆盖父类的run()
# a.duo_run()      # 这是多态的另一个好处，直接使用父类的函数
# b.run()		
# print(isinstance(b,TestJicheng1))   # 继承 TestJicheng1 类
# print(isinstance(b,TestJicheng2))   # 不继承 TestJicheng2 类




# 实例属性和类属性
class TestShili(object):
	name = '张三'

# a=TestShili()
# a.score = 12      # 增加实例属性
# print(a.name)
# print(TestShili.name)

# a.name = '李四'   # 添加实例属性
# print(a.name)     # 因为实例属性优先级比类属性优先级高，所以显示实例属性
# print(TestShili.name)		







# 面向对象高级编程
# 使用__slots__
class TestGaoji1(object):
	# 只允许添加 name 、 age  两个属性
	__slots__ = ('name', 'age')

class TestGaoji2(TestGaoji1):
	pass

# a=TestGaoji1()
# a.name = '12'
# a.age = '12'
# # a.score = 12  # 对属性限制了，不允许添加 score 属性

# b = TestGaoji2()
# b.score = 12    # __slots__ 只对当前类实例有作用，对继承没作用




# 使用 @property
class TestGaoji3(object):
	def set_age(self, value):
		if value != 100:
			raise ValueError('value not\'s 100')
		self._age = value

	def get_age(self):
		return self._age

# a = TestGaoji3()
# a.set_age(100)
# print(a.get_age())

class TestGaoji4(object):
	@property
	def age(self):
		return self._age
	
	@age.setter
	def score(self, value):
		if value != 1:
			raise ValueError('value cuowu')
		self._age = value

# a=TestGaoji4()
# a.score=1





# 多重继承
class TestGaoJicheng1(object):
	def print1(self):
		print('我是1号')

class TestGaoJicheng2(object):
	def print2(self):
		print('我是2号')

class TestGaoJicheng3(TestGaoJicheng1, TestGaoJicheng2):
	def print3(TestGaoJicheng1):
		TestGaoJicheng1.print1()

	def print4(TestGaoJicheng2):
		TestGaoJicheng2.print2()

# a=TestGaoJicheng3()
# a.print3()
# a.print4()




# 定制类
class TestGaoDingzhi(object):
	def __init__(self, name):
		self.name = name

	def __str__(self):
		return 'object name:%s'%self.name
	
	def __repr__(self):
		return 'test name:%s'%self.name

print(TestGaoDingzhi('张三'))
a = TestGaoDingzhi('李四')
print(a)










