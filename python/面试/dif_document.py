# 可变参数 ----可以传 列表、元祖。。。
def test_nums(*args):
	sun = 0
	for i in args:
		sun = sun + i
	return sun

# nums=[1,2,3]
# print(test_nums(*nums))






# 关键字参数 ----可以传 带参数名的可变参数
def test_kw(**kw):
	print(kw)

# aa={'name':'张三', 'age':12}
# test_kw(**aa)




# 命名关键字参数 ----对关键字参数中的参数名做限制
def test_kw2(**kw):
	if 'name' in kw:
		pass
	if 'age' in kw:
		pass
	print(kw)

# a={'name':'张三', 'age':12}
# test_kw2(**a)






# 递归函数 ----函数内部可以调用自身
def test_fact(n):
	if n == 1:
		return 1
	return n * test_fact(n-1)

'''
=> test_fact(5)
=> 5 * test_fact(4)
=> 5 * 4 * test_fact(3)
=> 5 * 4 * 3 * test_fact(2)
=> 5 * 4 * 3 * 2 * test_fatc(1)
=> 5 * 4 * 3 * 2 *1
=> 120
'''
print(test_fact(5))














