# 两数之和：传入一个数组和一个数字，判断数组中那两个数字相加等于传入的数字，返回数组下标
def sum_of_two_numbers(lists,num):
    '''
    type lists:list
    type num:int
    type return:list
    '''
    # 暴力破解
    '''
    for i in range(len(lists)):
        for j in range(len(lists)):
            if i != j and lists[i]+lists[j] == num:
                list2=[i,j]
                print(list2)
                return list2
    '''
    for i in range(len(lists)):
        j=num-lists[i]
        if j in lists and i != lists.index(j):
            list2=[i,lists.index(j)]
            print(list2)


# 无重复字符的最长子串
def test(s):
    if s is None or len(s) == 0:
        print('空')
    str_dict={}
    one_max=0
    start=0
    max_len=0
    for i in range(len(s)):
        if s[i] in str_dict and str_dict[s[i]] >= start:
            start=str_dict[s[i]]+1
        one_max=i - start+1
        str_dict[s[i]]=i
        max_len=max(max_len,one_max)
    print(max_len)


# 两数相加 list
def addTwoNumbers(l1,l2):
    l3,l4=l1.copy(),l2.copy()
    len_l3,len_l4=len(l1)-1,len(l2)-1
    # 反转列表
    for i in l1:
        l3[len_l3]=i
        len_l3-=1
    for i in l2:
        l4[len_l4]=i
        len_l4-=1
    # 转为整数
    l3=int(''.join([str(i) for i in l3]))
    l4=int(''.join([str(i) for i in l4]))
    # 计算结果，并反转结果
    l5=[int(i) for i in str(l3+l4)]
    l6,len_l6=l5.copy(),len(l5)-1
    for i in l5:
        l6[len_l6]=i
        len_l6-=1
    return l6


# 给出一个32位的有符号整数，对整数进行反转
def inversion_digital(nums):
    # 将整数转为列表
    list1=[i for i in str(nums)]
    # 判断是否是有符号的整数
    if list1[0].isdigit():
        # 无符号
        list2=list1.copy()
        index=1
        for i in list1:
            list2[len(list2)-index]=i
            index+=1
        num=int(''.join(list2))
    else:
        # 有符号
        list2=list1[1:].copy()
        index=1
        for i in list1[1:]:
            list2[len(list1[1:])-index]=i
            index+=1
        num=int(str('-')+str(int(''.join(list2))))
    if num > 2**31-1 or num < -2**31:
        return 0
    else:
        return num
    print(num)

# 最长回文子串
def return_max_s(s):
    dicts={}
    # 循环字符串，下标为key，属性为value
    for i,j in zip(range(len(s)),s):
        dicts[i]=j
    # 循环字典，value一致，将两个key存储到二维列表,第一个key要小于第二个key，避免重复
    list1=[ [i,j] for i in dicts for j in dicts if dicts[j] == dicts[i] if i < j]
    # 使用list1中的下标对s进行剪切，返回最大长度的字符串
    list2=[s[i[0]:i[1]+1] for i in list1  if len(s[i[0]:i[1]+1]) == max([ abs(i[1]-i[0]) for i in list1])+1]
    print(list2)


# 寻找两个有序数组的中位数
def two_list_center_num(list1,list2):
    # 合并两个list，并从小到大排序
    list3=sorted(list1+list2,reverse=False)
    # 求list长度
    len_list3=len(list3)
    if len_list3%2 == 0:
        index=int(len_list3/2)
        num=(list3[index]+list3[index-1])/2
    elif len_list3%2 == 1:
        index=int(len_list3/2-0.5)
        num=list3[index]
    print(list3)
    print(num)


# 罗马数字转整数
def romanToInt(s):
    '''
     I 可以放在 V (5) 和 X (10) 的左边，来表示 4 和 9。
    X 可以放在 L (50) 和 C (100) 的左边，来表示 40 和 90。 
    C 可以放在 D (500) 和 M (1000) 的左边，来表示 400 和 900。
    '''
    a = {'I':1, 'V':5, 'X':10, 'L':50, 'C':100, 'D':500, 'M':1000}
    num=0
    for i in range(len(s)):
        if i<len(s)-1 and a[s[i]] < a[s[i+1]]:
            num-=a[s[i]]
        else:
            num+=a[s[i]]
    print(num)

# 盛最多的水
def maxArea(height):
    # 暴力法
    '''
    l=[0]
    for i in range(len(height)):
        n=1
        for j in range(len(height)):
            if j>i+n:
                if height[i] < height[j]:
                    num=height[i]*(j-i)
                else:
                    num=height[j]*(j-i)
                if num > l[0]:
                    l[0]=num#.append(num)
                print(i,j,n,num,l)
                # n+=1
    return l[0]
    '''
    num,l,r=0,0,len(height)-1
    while l<r:
        num=max(num,min(height[l],height[r])*(r-l))
        print(num,l,r)
        if height[l] < height[r]:
            l+=1
        else:
            r-=1
    print(num)
    return num


# 整数转罗马数字    
def intToRoman( num: int) -> str:
    l=[
        ['','I','II','III','IV','V','VI','VII','VIII','IX'],
        ['','X','XX','XXX','XL','L','LX','LXX','LXXX','XC'],
        ['','C','CC','CCC','CD','D','DC','DCC','DCCC','CM']
    ]
    s=''
    if len(str(num)) == 4:
        s+='M'*(num//1000)
        num=int(str(num)[1:])
    if len(str(num)) == 3:
        s+=l[2][num//100]
        num=int(str(num)[1:])
    if len(str(num)) == 2:
        s+=l[1][num//10]
        num=int(str(num)[1:])
    if len(str(num)) == 1:
        s+=l[0][num]
    return s



# 三数之和
def threeSum(nums):
    for i in nums:
        c=nums.count(i)
        if c > 3:
            n=0
            while n < c-3:
                nums.pop(nums.index(i))
                n+=1
    l=[]
    for i in range(len(nums)):
        for j in range(len(nums)):
            for k in range(len(nums)):
                if i<j<k and nums[i]+nums[j]+nums[k] == 0 :
                    new=[nums[i],nums[j],nums[k]]
                    new.sort(reverse=False)
                    if new not in l :
                        l.append(new)
    l.sort(reverse=False)
    print(l)






























