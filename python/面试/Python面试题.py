# 打印9*9乘法表 
def chengfa_9_9():
    for i in range(1,10):
        for j in range(1,10):
            print('%d*%d=%2d '%(j,i,i*j),end='')
            if j == i :
                print('\n')
                break

# 打印乘法表
def chengfa(x,y):
    for i in range(1,x+1):
        for j in range(1,y+1):
            print('%d*%d=%2d '%(j,i,i*j),end='')
            if j == i :
                print('\n')
                break


# 1:1234四个数字能组成多少个不重复的三位数
def py1():
    list=[1,2,3,4]
    num=0
    for i in list:
        for j in list:
            for x in list:
                if x !=i and x!=j and i !=j:
                    num+=1
                    print(str(i)+str(j)+str(x))
    print("总共："+str(num)+"次")


# 2:计算利润
def py2(num):
    a=10*0.1
    b=10*0.075
    c=20*0.05
    if num <= 10:
        profits=num*0.1
    if num >10 and num <=20:
        b=(num-10)*0.075
        profits=a+b
    if num >20 and num <=40:
        c=(num-20)*0.05
        profits=a+b+c
    if num > 40 and num <= 60 :
        d=(num-40)*0.03
        profits=a+b+c+d
    print(profits)
        


# 3:一个整数加上100后是完全平方数，在加上268还是完全平方数，求该数
def py3():
    import math
    for i in range(10000):
        x=int(math.sqrt(i+100))
        y=int(math.sqrt(i+268))
        if (x*x == i+100) and (y*y == i+268):
            print(i)
    
# 4: 输入三个数，有小到大输出
def py4(x,y,z):
    if x > y:
        x,y=y,x
    if y >z :
        y,z=z,y
    if x>y:
        x,y=y,x
    print(x,"、",y,"、",z)


# 5: 打印字母c
def py5():
    print('*'*5)
    print('*')
    print('*')
    print('*')
    print('*'*5)


# 6: 打印所有的水仙花数
def py6():
    for num in range(100,1000):
        if len(str(num)) == 3:
            if int(str(num)[:1])**3 + int(str(num)[1:2])**3 + int(str(num)[2:])**3 == num:
                print(num)


# 7: 判断一句话中有多少个字母、数字、空格
def py7(strs):
    a,b,c=0,0,0
    for i in strs:
        if i.isalpha():
            a+=1
        if i.isdigit():
            b+=1
        if i.isspace():
            c+=1
    print("字母有："+str(a)+"个")
    print("数字有："+str(b)+"个")
    print("空格有："+str(c)+"个")



# Python基础篇
#====================================================================

# 1.Python解释器种类及特点
'''
CPython：c语言开发，使用最广的解释器
IPython：基于CPython智商的一个交互式计时器，交互方式增强，功能和CPython一样
PyPy：采用JIT技术，对Python代码进行动态编译，提高执行效率
JPython：运行在Java上的解释器，直接把Python代码编译成Java字节码执行
IronPython：运行在微软 .NET 平台上的解释器，把Python编译成 .NET 的字节码
'''


# 2.简述解释型和编译型编程语言
'''
解释型：可以直接运行，逐条翻译逐条运行
编译型：把源码全部编译成二进制代码的运行程序，然后运行这个程序
总结：
解释型：执行速度慢，效率低，依赖解释器，跨平台好
编译型：执行速度快，效率高，依赖编译器，跨平台差
'''


# 3.位于字节
'''
1字节等于8位   1byte = 8bit
'''


# 4.进制转换
'''
2进制：bin   8进制：oct    10进制：int    16进制：hex
2_8 => oct(int(x,2))      2_10 => int(x,2)         2_16 => hex(int(x,2))
8_2 => bin(int(x,8))      8_10 => int(x,8)         8_16 => hex(int(x,8))
10_2 => bin(int(x,10))    10_8 => oct(int(x,10))   10_16 => hex(int(x,10))
16_2 => bin(int(x,16))    16_8 => oct(int(x,16))   16_10 => int(x,16)

二进制转十进制：v='0b1111011' =>int(v[2:],2)            ==>123
十进制转二进制：v=18          =>bin(int('18',10))[2:]   ==>10010
八进制转十进制：v='011'       =>int('011',8)            ==>9
十进制转八进制：v=30          =>oct(int('30',10))[2:]   ==>36
十六进制转十进制：v='12'      =>int('12',16)            ==>18
十进制转十六进制：v=87        =>hex(int('87',10))[2:]   ==>57
'''


# 5.IP转为整数
def py8(str_ip):
    h = []
    s = str_ip.split('.')
    for i in s:
        a = bin(int(i))[2:]
        a = a.zfill(8)
        h.append(a)
        g = ''.join(h)
        e = int(g, 2)
    print(e)




# 6.字节码和机器码的区别
'''
机器码：学名机器语言指令，有时也被称为原生吗，是电脑cpu可以直接解读的数据
字节码：是一种包含执行程序，由一序列 op 代码数据对 组成的二进制文件。需要转译后才能成为机器码的中间码
'''


# 7.三元运算规则以及应用场景
def py9():
    a, b = 2, 3
    c = a if a > 1  else b
    print(c)



# 8.xrange 与 range 的区别
def py10():
    ''' python2中：
    都是用来循环的
    range 支持列表生成式， xrange不支持
    xrange支持步长， range不支持
        Python3中对range升级，支持步长，取消xrange
    '''
    list1 = [i for i in range(0, 10, 2)]
    print(list1, list2)



'''列表'''
# list1：对list去重，写出具体过程
def list1():
    alist = [1, 2, 3, 1, 2]
    new_list = []
    for i in alist:
        if i not in new_list: 
            new_list.append(i)
    print(new_list)


# list2：如何实现 "1,2,3" 变成 ["1","2","3"]
def list2():
    str1 = "1,2,3"
    return_list = [ i for i in str1 if i != ',']
    print(return_list)


# list3：给定两个 list，A 和 B，找出相同元素和不同元素
def list3():
    list_a = [1, 3, 5, 6, 7, 8]
    list_b = [2, 4, 6, 7, 8, 9]
    xiangtong = set(list_a) & set(list_b)
    butong = set(list_a) ^ set(list_b)
    print(xiangtong,'\n',butong)


# list4：[[1,2],[3,4],[5,6]]一行代码展开该列表，得出[1,2,3,4,5,6]
def list4():
    a =[j for i in [[1, 2], [3, 4], [5, 6]] for j in i]
    print(a)


# list5：合并列表[1,5,7,9]和[2,2,6,8]
def list5():
    list_a = [1, 5, 7, 9]
    list_b = [2, 2, 6, 8]
    list_c = list_a + list_b
    list_c.sort(reverse = False)
    print(list_c)

# list6:如何打乱一个列表的元素？
def list6():
    import random
    a = [1, 2, 3, 4, 5]
    random.shuffle(a)
    print(a)



'''
字典
'''
# dict1；字典操作中 del 和 pop 有什么区别
'''
clear：清空字典内所有元素
pop： 删除指定key
'''

# dict2:按照字典的内的年龄排序
def dict2():
    dd = [
        {'name':'aaa', 'age':18},
        {'name':'bbb', 'age':11},
        {'name':'ccc', 'age':26},
    ]
    dd.sort(key = lambda x : x['age'])
    print(dd)



# dict3:请合并下面两个字典 a = {"A":1,"B":2},b = {"C":3,"D":4}
def dict3():
    a = {'a':1, 'b':2}
    b = {'c':3, 'd':4}
    c = {}
    c.update(a)
    c.update(b)

    c1 = dict(a, **b)
    print(c, c1)



# dict4:如何使用生成式的方式生成一个字典，写一段功能代码
def dict4():
    a = [1, 2, 3]
    b = ['a', 'b', 'c']
    c = { k:v for k, v in zip(b, a)}
    print(c)



# dict5:如何把元组("a","b")和元组(1,2)，变为字典{"a":1,"b":2}
def dict5():
    a, b, c = ('a', 'b'), (1, 2), {}
    for k, v in zip(a, b):
        c[k] = v
    print(c)






