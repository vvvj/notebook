# 列表生成式
num = '0123456789'
data={'name':'张三', 'age':'12'}
list1 = [x for x in range(10)]
list2 = [ i0 + i1 for i0 in num for i1 in num]
list3 = [ x + '=' + y for x, y in data.items()]
# print(list1)
# print(list2)
# print(list3)






# 生成器
g = ( x * x for x in range(10))
# print(g)
# for i in g:
# 	print(i)



def fib(max):
	n, a, b = 0, 0, 1
	while n < max:
		print(b)
		a, b = b, a+b
		n+=1

# fib(6)

def fib_yield(max):
	n, a, b = 0, 0, 1
	while n < max:
		yield b
		a, b = b, a+b
		n+=1
	return 'done'

# f=fib_yield(6)
# print(f)
# print(next(f),next(f))


def odd():
	print(11)
	yield 1
	print(12)
	yield 2

# print(odd())
# print(next(odd()))



# 迭代器
# 能不断被  next() 函数调用并返回下一个值的对象叫做迭代器



