import requests
from bs4 import BeautifulSoup
from time import sleep
import os

name=0
for num in range(1,20):
	url='https://www.mzitu.com/jiepai/comment-page-%d/#comments'%num
	res=requests.get(url).text

	# 初始化
	soup=BeautifulSoup(res,'lxml')

	# 获取title，并创建文件夹
	title=soup.title.text
	if os.path.exists(title) is False:
		os.mkdir(title)

	# 解析图片地址
	img_urls=soup.select('ul li div p img')
	
	for i in img_urls:
		# 生成图片名
		path=title+'/%d.jpg'%name
		
		# 访问图片链接，二进制返回
		img=requests.get(i["data-original"]).content
		# 写入文件
		with open(path,'wb') as f:
			f.write(img)
		print('正在下载：'+path)
		name+=1
	# 没下载一页暂定2s
	sleep(2)
	print('正在下载第%d页'%num)
print('下载完成')