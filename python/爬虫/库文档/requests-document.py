'''
requests  库文档
https://2.python-requests.org//zh_CN/latest/user/quickstart.html
'''

# 安装 requests 库
pip install requests


# get请求，带参数的get请求
import requests

url='https://www.xxx.com'
res=requests.get(url)

params={'key1' : 'value1', 'key2' : 'value2'}
res=requests.get(url,params=params)


# post请求，带参数的post请求
import requests

url='https://www.xxx.com'
res=requests.post(url)

data={'key1':'value1', 'key2':'value2'}
res=requests.post(url,data=data)


# 添加请求头
url='https://www.xxx.com'
headers={'user-agent': 'my-app/0.0.1'}
res=requests.post(url, headers=headers)


# 添加 cookies
url='https://www.xxx.com'
cookies=dict(name='zhangsan')
res=requests.post(url,cookies=cookies)


# 返回信息
res.encoding  # 返回编码
res.url       # 返回url
res.text      # 返回文本信息
res.content   # 返回二进制信息
res.json()    # 返回json格式数据
res.status_code  # 返回状态码
res.headers   # 返回响应头
res.cookies['name']  # 返回指定cookies信息




# 将文本流保存到文件
with open('a.txt','wb')as fd:
	for chunk in res.iter_content(chunk_size):
		fd.write(chunk)




# 上传文件
url='https://www.xxx.com'
files={'file': open('a.txt','rb')}
res=requests.post(url,files=files)




# 超时
url='https://www.xxx.com'
res=requests.post(url, timeout=0.001)