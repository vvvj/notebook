'''
Beautiful Soup 库文档
https://www.crummy.com/software/BeautifulSoup/bs4/doc.zh/
'''

# 安装
# pip install beautifulsoup4
# pip install lxml


# 解析器，使用方法，优劣势
'''
1.Python标准库
使用方法：BeautifulSoup(html,'html.parser')
优势：python内置标准库；执行速度适中；文档容错能力强
劣势：Python2.7.3 or 3.2.3 前的版本中文档容错能力差

2.lxml html解析器   
使用方法：BeautifulSoup(html,'lxml')
优势：速度快；文档容错能力强
劣势：需要安装c语言库

3.lxml xml解析库
使用方法：BeautifulSoup(html,['lxml','xml'])
优势：速度快；唯一支持xml的解析库
劣势：需要安装c语言库

4.html5lib
使用方法：BeautifuleSoup(html,'html5lib')
优势：最好的容错性；以浏览器的方式解析文档；生成html5格式的文档
劣势：速度慢；不依赖外部扩展
'''

# 导入
from bs4 import BeautifulSoup


# 示例 HTML
html='''
<html>
	<head>
		<title>测试</title>
	</heda>
	<body>
		<p class="p_class">p1</p>
		<p id="xx">p2</p>
		<p id="xxx">p3</p>
		<div><b>test_b</b></div>
	</body>
</html>
'''

# 解析
soup=BeautifulSoup(html, 'lxml')

title=soup.title               # 获取HTML的 title
title_name=soup.title.name     # 获取title的name
title_text=soup.title.text     # 获取title的text

title_parent_name=soup.title.parent.name   # 获取title 父级标签的 name

p_no1=soup.p                   # 获取第一个 p 标签
p_no1_class=soup.p['class']    # 获取 p 标签的class
p_all=soup.find_all('p')       # 获取所有 p 标签

html_text=soup.get_text()      # 获取HTML中所有文字



# find_all(name,attrs,recursive,text,**kwargs)
'''
soup.find_all('title')
soup.find_all('p','p_class')
soup.find_all(id='xx')
soup.find_all(id=True)
soup.find_all(attrs={"data-foo":"value"})
soup.find_all('p',class_='p_class')
soup.find_all('p',limit=2)
soup.find_all('div',recursive=False)
'''

# find(name,attrs,recursive,text,**kwargs)

# css选择器
'''
soup.select('title')
soup.select('body p')
soup.select('head > title')
soup.select('p > #xx')
soup.select('#xx')
soup.select('.p_class')
soup.select('a[href]')
'''

