from multiprocessing.managers import BaseManager
import requests
from bs4 import BeautifulSoup
import csv


# 从BaseManage 继承的 QueueManager
class QueueManager(BaseManager):
	pass

# 使用QueueManager注册获取Queue的方法名称
QueueManager.register('get_url_queue')
QueueManager.register('get_over_queue')


# 连接服务器
m = QueueManager(address = ('127.0.0.1', 5000), authkey = b'abc')
m.connect()


# 获取Queue对象
url = m.get_url_queue()
over = m.get_over_queue()


# 从url队列中获取详细页面url，爬取数据存到csv文件中
num, cuowu_list = 0, []
while (not url.empty()):
	# 获取详细页面url
	xiangxi_url = url.get()
	# 访问url
	res = requests.get(xiangxi_url)
	# 解析，获取数据
	soup = BeautifulSoup(res.text, 'lxml')
	content = soup.find('div', class_ = 'content')
	# 存入csv文件
	try:
		with open('csv.csv', 'a', newline = '', encoding = 'utf-8') as f:
			w = csv.writer(f)
			w.writerow([content.text])

		num += 1
		print('已存储%d条'%num)
	except:
		cuowu_list.append(xiangxi_url)

# 告诉服务端，我操作完了，可以关闭服务了
over.put('over')
print('over')
print('存储错误：', cuowu_list)

