from multiprocessing.managers import BaseManager
from multiprocessing import freeze_support
import queue
import requests
from bs4 import BeautifulSoup
from time import sleep

# 设置队列
url_queue = queue.Queue()
over_queue = queue.Queue()

def get_url():
	return url_queue

def get_over():
	return over_queue

# 从BaseManage 继承的 QueueManager
class QueueManager(BaseManager):
	pass

def win_run():
	QueueManager.register('get_url_queue', callable =  get_url)
	QueueManager.register('get_over_queue', callable = get_over)

	manager = QueueManager(address = ('127.0.0.1', 5000), authkey = b'abc')
	manager.start()


	try:
		url = manager.get_url_queue()
		over = manager.get_over_queue()

		# 添加任务
		pa_url_list = ['https://www.qiushibaike.com/textnew/page/%d'%i for i in range(1, 36)]
		num = 0
		for page_url in pa_url_list:
			res = requests.get(page_url)
			soup = BeautifulSoup(res.text, 'lxml')
			for i in soup.find_all('a', class_ = 'contentHerf'):
				num += 1
				print(num, 'https://www.qiushibaike.com' + i['href'])
				url.put('https://www.qiushibaike.com' + i['href'])
		# xx
		print('等待 xx 。。。')
		for i in range(1,2):
			xx = over.get()
			print(xx)
	except :
		print('遇到了一个错误')
	finally:
		manager.shutdown()

if __name__ == '__main__':
	freeze_support()
	win_run()


