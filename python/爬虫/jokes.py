import pymysql
# pip install PyMySQL

import requests
import urllib
from bs4 import BeautifulSoup
import time


def test_db():
    db=pymysql.connect('127.0.0.1','root','root','joke')
    cursor=db.cursor()
    sql='select version()'
    cursor.execute(sql)
    db.commit()
    db.close()



# 此函数用于爬取:段子网__经典段子
# 20190114  共518条

'''
table_name:jokes_classic
id int(11) pk
content text
'''
def jokes_classic():
    # 循环出52个url
    for page in range(1,53):
        url='http://duanziwang.com/category/%E7%BB%8F%E5%85%B8%E6%AE%B5%E5%AD%90/{}/'.format(page)
        res=requests.get(url)
        
        soup=BeautifulSoup(res.text,'lxml')

        # 打印出每条段子
        for i in soup.select('.post-content'):
            # 存储到mysql数据库
            db=pymysql.connect('127.0.0.1','root','root','joke')
            cursor=db.cursor()
            sql="insert into jokes_classic(content) values('%s')"%i.text
            cursor.execute(sql)
            db.commit()
            db.close()
            print('正在存储中...')
    print('存储完成!')
            




# 段子网__一句话段子
# 20190114 共618条
'''
table_name:jokes_a_word_of_jokes
id int(11) pk
content text
'''
def jokes_a_word_of_jokes():
    # 循环出62个 url
    for page in range(1,63):
        url='http://duanziwang.com/category/%E4%B8%80%E5%8F%A5%E8%AF%9D%E6%AE%B5%E5%AD%90/{}/'.format(page)
        res=requests.get(url)

        soup=BeautifulSoup(res.text,'lxml')
        
        # 打印
        for i in soup.find_all("h1", class_="post-title"):
            # 存储到数据库
            db=pymysql.connect('127.0.0.1','root','root','joke')
            cursor=db.cursor()
            sql="insert into jokes_a_word_of_jokes(content) values('%s')"%i.text
            cursor.execute(sql)
            db.commit()
            db.close()
            print('正在存储中。。。')
    print('存储完成！')





# 糗事百科--文字
# 20190114 共25*13=354条
'''
table_name:jokes_qiubai
id int(11) pk
content text utf8bm4
'''
def jokes_qiubai_wenzi():
    for page in range(1,14):
        url='https://www.qiushibaike.com/text/page/{}/'.format(page)
        res=requests.get(url)

        soup=BeautifulSoup(res.text,'lxml')

        num=int(1)
        for i in soup.select('.content'):
            db=pymysql.connect('127.0.0.1','root','root','joke')
            cursor=db.cursor()
            sql="insert into jokes_qiubai(content) values('%s')"%i.text
            cursor.execute(sql)
            db.commit()
            db.close()
            print('正在存储第%d条...'%num)
            num += 1
        print('第%d页!'%page)
        time.sleep(3)
    print('存储完毕')
            
            


# 捧腹网--段子
# 20190114 共495条
def jokes_pengfu_jokes():
    # 循环出50个url
    for page in range(1,51):
        url='https://www.pengfu.com/xiaohua_'+str(page)+'.html'
        
        user_agent='Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'
        Cookie='think_var=zh-cn; __guid=42723747.4131045460588046300.1547449649459.5227; Hm_lvt_23eff1d381ebd0e40be18a8be49a2ced=1547449655; monitor_count=5; Hm_lpvt_23eff1d381ebd0e40be18a8be49a2ced=1547451009'
    
        headers = {'User-Agent':user_agent,'Cookie':Cookie}
        res=requests.get(url,headers=headers)
    
        soup=BeautifulSoup(res.text,'lxml')
        num=int(1)
        for i in soup.find_all('div',class_='content-img clearfix pt10 relative'):
            db=pymysql.connect('127.0.0.1','root','root','joke')
            cursor=db.cursor()
            sql="insert into jokes_pengfu_duanzi(content) values('%s')"%i.text
            cursor.execute(sql)
            db.commit()
            db.close()
            print('正在存储第%d条...'%num)
            num += 1
        print('第'+ str(page)+'页')


# 别逗了--段子
# 20190114 共26951条
def jokes_biedoule():
    for page in range(1715,1802):
        url='https://www.biedoul.com/wenzi/{}/'.format(page)

        cookie='PHPSESSID=0tgjphmuorh0ndu4ios9f1rsc0; __guid=236152199.3538985630006902300.1547451157293.627; Hm_lvt_371c0214d89fec70079f78acce5c6fc7=1547451158; _ga=GA1.2.1572574725.1547451158; _gid=GA1.2.1527803320.1547451158; monitor_count=21; Hm_lpvt_371c0214d89fec70079f78acce5c6fc7=1547451413'
        user_agent='Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'

        headers={'user-agent':user_agent,'cookie':cookie}

        res=requests.get(url,headers=headers)

        soup=BeautifulSoup(res.text,'lxml')
        num=int(1)
        for i in soup.find_all('p',class_='fonts'):
            db=pymysql.connect('127.0.0.1','root','root','joke')
            cursor=db.cursor()
            sql="insert into jokes_biedoule(content) values('%s')"%i.text
            cursor.execute(sql)
            db.commit()
            db.close()
            print('正在存储第%d条...'%num)
            num += 1
        print('第'+ str(page)+'页')
        #time.sleep(2)
    print('存储完成!')



# 笑话经典--冷笑话
def jokes_jingdian_lengxiaohua():
    for page in range(1,126):
        url='http://xiaohua.zol.com.cn/lengxiaohua/{}.html'.format(page)
        res=requests.get(url)

        soup=BeautifulSoup(res.text,'lxml')

        for i in soup.find_all('a',class_='all-read'):
            page_url='http://xiaohua.zol.com.cn'+ i.get('href')
            
            headers={
                'User-Agent':'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36',
                'Cookie':'ip_ck=4sOD4vPwj7QuNTQyNTk3LjE1MzgyMDY4MTA%3D; __guid=3737785.2490407257525754000.1542025930185.0867; bdshare_firstime=1542025931003; _ga=GA1.3.710307911.1542025931; z_pro_city=s_provice%3Dguangdong%26s_city%3Dshenzhen; userProvinceId=30; userCityId=348; userCountyId=0; userLocationId=24; _gid=GA1.3.1490211501.1547439673; lv=1547514603; vn=9; Hm_lvt_ae5edc2bc4fc71370807f6187f0a2dd0=1547439673,1547459194,1547514604; monitor_count=19; Hm_lpvt_ae5edc2bc4fc71370807f6187f0a2dd0=1547515151; questionnaire_pv=1547510405; z_day=izol104957%3D1%26izol104963%3D2%26izol104955%3D1'
                }
            page_res=requests.get(page_url,headers=headers)

            page_soup=BeautifulSoup(page_res.text,'lxml')

            num=1
            for j in page_soup.select('.article-text'):
                db=pymysql.connect('127.0.0.1','root','root','joke')
                cursor=db.cursor()
                sql="insert into jokes_jingdian_lengxiaohua(content) values('%s')"%j.text
                cursor.execute(sql)
                db.commit()
                db.close()
                print('正在存储第%d条...'%num)
                num += 1
        print('第'+str(page)+'页')
    print('存储完毕!')



# 笑话大全--幽默
def jokes_jindian_youmo():
    for page in range(1,201):
        url='http://xiaohua.zol.com.cn/youmo/'+ str(page) +'.html'
        headers={
            'User-Agent':'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36',
            'Cookie':'__guid=3737785.4344593354012086000.1547515792597.247; z_pro_city=s_provice%3Dguangdong%26s_city%3Dshenzhen; userProvinceId=30; userCityId=348; userCountyId=0; userLocationId=24; lv=1547515794; vn=1; ip_ck=5s+I4vz3j7QuMTg5NTYwLjE1NDc1MTU4MzY%3D; Hm_lvt_ae5edc2bc4fc71370807f6187f0a2dd0=1547515795; _ga=GA1.3.2041005026.1547515797; _gid=GA1.3.460726372.1547515797; bdshare_firstime=1547515797927; z_day=izol104237%3D9%26izol104957%3D10%26izol104963%3D10%26izol104962%3D10%26izol104955%3D7%26ixgo20%3D1; monitor_count=62; Hm_lpvt_ae5edc2bc4fc71370807f6187f0a2dd0=1547516904; questionnaire_pv=1547510462'
            }
        res=requests.get(url,headers=headers)
        soup=BeautifulSoup(res.text,'lxml')
        for i in soup.find_all('a',class_='all-read'):
            page_url='http://xiaohua.zol.com.cn'+ i.get('href')
            page_headers={
                'User-Agent':'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36',
                'Cookie':'__guid=3737785.4344593354012086000.1547515792597.247; z_pro_city=s_provice%3Dguangdong%26s_city%3Dshenzhen; userProvinceId=30; userCityId=348; userCountyId=0; userLocationId=24; lv=1547515794; vn=1; ip_ck=5s+I4vz3j7QuMTg5NTYwLjE1NDc1MTU4MzY%3D; Hm_lvt_ae5edc2bc4fc71370807f6187f0a2dd0=1547515795; _ga=GA1.3.2041005026.1547515797; _gid=GA1.3.460726372.1547515797; bdshare_firstime=1547515797927; z_day=izol104237%3D9%26izol104957%3D10%26izol104963%3D10%26izol104962%3D10%26izol104955%3D7%26ixgo20%3D1; questionnaire_pv=1547510463; Hm_lpvt_ae5edc2bc4fc71370807f6187f0a2dd0=1547517198; 771d19c5863c542b58d9d0c08c0527c5=13t2dj6g2k62v14g1fk%7B%7BZ%7D%7D%25E6%259F%25A5%25E7%259C%258B%25E5%2585%25A8%25E6%2596%2587%25C2%25BB%7B%7BZ%7D%7Dnull; 8457bb92cddccc63c9d0e5b73823f710=13t2dj6g2k62v14g1fk%7B%7BZ%7D%7D%25E6%259F%25A5%25E7%259C%258B%25E5%2585%25A8%25E6%2596%2587%25C2%25BB%7B%7BZ%7D%7Dnull; MyZClick_771d19c5863c542b58d9d0c08c0527c5=/html/body/div%5B6%5D/div/ul/li/div%5B3%5D/a/; MyZClick_8457bb92cddccc63c9d0e5b73823f710=/html/body/div%5B6%5D/div/ul/li/div%5B3%5D/a/; monitor_count=64'
                }
            page_res=requests.get(page_url,headers=page_headers)
            page_soup=BeautifulSoup(page_res.text,'lxml')
            num=int(1)
            for j in page_soup.select('.article-text'):
                db=pymysql.connect('127.0.0.1','root','root','joke')
                cursor=db.cursor()
                sql="insert into jokes_jindian_youmo(content) values('%s')"%j.text
                cursor.execute(sql)
                db.commit()
                db.close()
                print('正在存储第{}条...'.format(num))
                num+=1
        print('第'+ str(page) +'页')
    print('存储完毕！')


# 笑话大全--综合
def jokes_jindian_zonghe():
    for page in range(1,449):
        url='http://xiaohua.zol.com.cn/zonghe/'+ str(page) +'.html'
        headers={
            'User-Agent':'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36',
            'Cookie':'__guid=3737785.4344593354012086000.1547515792597.247; z_pro_city=s_provice%3Dguangdong%26s_city%3Dshenzhen; userProvinceId=30; userCityId=348; userCountyId=0; userLocationId=24; lv=1547515794; vn=1; ip_ck=5s+I4vz3j7QuMTg5NTYwLjE1NDc1MTU4MzY%3D; Hm_lvt_ae5edc2bc4fc71370807f6187f0a2dd0=1547515795; _ga=GA1.3.2041005026.1547515797; _gid=GA1.3.460726372.1547515797; bdshare_firstime=1547515797927; z_day=izol104237%3D10%26izol104957%3D10%26izol104963%3D10%26izol104962%3D10%26izol104955%3D7%26ixgo20%3D1; monitor_count=79; Hm_lpvt_ae5edc2bc4fc71370807f6187f0a2dd0=1547522241; _gat=1; questionnaire_pv=1547510478'
            }
        res=requests.get(url,headers=headers)
        soup=BeautifulSoup(res.text,'lxml')
        for i in soup.find_all('a',class_='all-read'):
            page_url='http://xiaohua.zol.com.cn'+ i.get('href')
            page_headers={
                'User-Agent':'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36',
                'Cookie':'__guid=3737785.4344593354012086000.1547515792597.247; z_pro_city=s_provice%3Dguangdong%26s_city%3Dshenzhen; userProvinceId=30; userCityId=348; userCountyId=0; userLocationId=24; lv=1547515794; vn=1; ip_ck=5s+I4vz3j7QuMTg5NTYwLjE1NDc1MTU4MzY%3D; Hm_lvt_ae5edc2bc4fc71370807f6187f0a2dd0=1547515795; _ga=GA1.3.2041005026.1547515797; _gid=GA1.3.460726372.1547515797; bdshare_firstime=1547515797927; z_day=izol104237%3D10%26izol104957%3D10%26izol104963%3D10%26izol104962%3D10%26izol104955%3D7%26ixgo20%3D1; monitor_count=81; questionnaire_pv=1547510480; Hm_lpvt_ae5edc2bc4fc71370807f6187f0a2dd0=1547522603; _gat=1'
                }
            page_res=requests.get(page_url,headers=page_headers)
            page_soup=BeautifulSoup(page_res.text,'lxml')
            for j in page_soup.select('.article-text'):
                db=pymysql.connect('127.0.0.1','root','root','joke')
                cursor=db.cursor()
                sql="insert into jokes_jindian_zonghe(content) values('%s')"%j.text
                cursor.execute(sql)
                db.commit()
                db.close()
                print('正在存储...')
        print('第'+ str(page)+'页！')
    print('存储完毕!')






    






















