'''
title: 爬取表情党中表情
author: 十一
datatime: 20191120 112000
'''


import requests, re
from bs4 import BeautifulSoup


# 访问网页
url = 'http://qq.yh31.com/zjbq/2920180_2.html'
res = requests.get(url)


# 解析
soup = BeautifulSoup(res.text, 'lxml')
img_list = soup.find_all('img', src = re.compile("tp"))                 # 1.先解析出正确的 img 标签
img_url = ['https://qq.yh31.com' + i.get('src') for i in img_list]      # 2.拼接成对应的 url



# 下载
for k, v in zip(list(range(len(img_url))), img_url):
    # 获取图像二进制数据
    res = requests.get(v)

    # 写入文件
    with open('img/%d.gif'%k, 'wb')as f:
        f.write(res.content)
    
print('下载完成！')