import threading
import os

'''
全局变量local_school就是一个ThreadLocal对象，每个Thread对它都可以读写student属性，但互不影响。
可以把local_school看成全局变量，但每个属性如local_school.student都是线程的局部变量，可以任意读写而互不干扰，也不用管理锁的问题，ThreadLocal内部会处理
ThreadLocal变量虽然是全局变量，但每个线程都只能读写自己线程的独立副本，互不干扰。ThreadLocal解决了参数在一个线程中各个函数之间互相传递的问题。
'''

local_shool = threading.local()

def process_student():
	std = local_shool.student 
	print('hello  %s (in %s)  %s'%(std, threading.current_thread().name, os.getpid()))

def process_thread(name):
	local_shool.student = name
	process_student()

t1 = threading.Thread(target = process_thread, args = ('AA',), name = '测试一')
t2 = threading.Thread(target = process_thread, args = ('BB',), name = '测试二')

t1.start()
t2.start()
t1.join()
t2.join()
