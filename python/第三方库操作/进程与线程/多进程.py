# linux 使用 fork
# Windows 使用 multiprocessing

from multiprocessing import Process
from multiprocessing import Pool
from multiprocessing import Queue
import subprocess
import os
import time 
import random

# multiprocessing  ----  Process
'''
def test_proc(name):
	print('run chid process %s (%s)'%(name, os.getpid()))

if __name__ == '__main__':
	print('Parent process %s'%os.getpid())

	p = Process(target = test_proc, args = ('zhangsna',))

	print('start')
	p.start()
	p.join()
	print('end')
'''




# Pool 批量创建子进程
'''
def long_time_task(name):
	print('run task %s (%s)'%(name, os.getpid()))

	start = time.time()
	time.sleep(random.random() * 3)
	end = time.time()
	print('task %s runs %0.2f seconds'%(name, (end - start)))

if __name__ == '__main__':
	print('Parent process %s'%os.getpid())
	p = Pool(6)  # 同时跑 6 个进程

	for i in range(9):
		p.apply_async(long_time_task, args = (i,))

	print('waiting for all ...')
	p.close()
	p.join()
	print('all subprocesses done')
'''




# subprocess  子进程
'''
print('$ nslookup www.baidu.com')
r = subprocess.call(['nslookup', 'www.python.org'])
print('exit code:', r)
'''



# Queue  进程间通信
'''
def write(q):
	print('write pid : %s'%os.getpid())

	for i in ['a', 'b', 'c']:
		print('put value: %s'%i)
		q.put(i)
		time.sleep(random.random())

def read(q):
	print('read pid: %s'%os.getpid())

	while True:
		if not q.empty():
			i = q.get(True)
			print('get value : %s'%i)
			time.sleep(1)
		else:
			break

if __name__ == '__main__':
	q = Queue()

	pw = Process(target = write, args = (q,))
	pr = Process(target = read, args = (q,))

	pw.start()
	pr.start()

	pw.join()
	pr.join() 
'''

