import threading
import os
import time


# 多线程基本应用
class Test1_Threading(object):
	def __init__(self):
		print('start : %s'%threading.current_thread().name)
		t = threading.Thread(target = self.loop, name = 'TestTread')
		t.start()
		t.join()
		print('end : %s'%threading.current_thread().name)

	def loop(self):
		print('thread %s is run'%threading.current_thread().name)
		n = 0
		while n < 5:
			n += 1
			print('%s => %s'%(threading.current_thread().name, n))
			time.sleep(1)
		print('end')




# Lock  
# ps:多线程中，变量由所有线程共享
class Test2_Treading(object):
	def __init__(self):
		self.balance = 0
		self.lock = threading.Lock()

		t1 = threading.Thread(target = self.new_run_thread, args = (5,))
		t2 = threading.Thread(target = self.new_run_thread, args = (8,))
		t1.start()
		t2.start()
		t1.join()
		t2.join()
		print(self.balance, os.getpid())

	def change_it(self, n):
		self.balance = self.balance + n
		self.balance = self.balance - n


	# 错误示例，数值足够大的时候，数据会错误
	def run_thread(self, n):
		for i in range(1000000):
			self.change_it(n)

	# 加锁，避免数值多大，数据错误
	def new_run_thread(self, n):
		for i in range(1000000):
			self.lock.acquire()
			try:
				self.change_it(n)
			finally:
				self.lock.release()



	