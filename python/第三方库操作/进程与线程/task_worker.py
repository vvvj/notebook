import time
from multiprocessing.managers import BaseManager

# 创建类似QUeueManager
class QueueManager(BaseManager):
	pass

# 使用QueueManager注册获取Queue的方法名称
QueueManager.register('get_task_queue')
QueueManager.register('get_result_queue')

# 连接服务器
m = QueueManager(address = ('127.0.0.1', 5001), authkey = b'abc')
m.connect()

# 获取Queue对象
task = m.get_task_queue()
result = m.get_result_queue()

# 从task 取任务，把结果写入 result
while (not task.empty()):
	text = task.get()
	print('task 结果  ： %s'%text)

	result.put('啊哈哈哈，我是从task中取出来存到result的' + str(text))

print('over')