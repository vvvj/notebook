from multiprocessing.managers import BaseManager
from multiprocessing import freeze_support
from multiprocessing import Queue

# 任务个数
task_number = 10 

# 收发队列
task_queue = Queue(task_number)
result_queue = Queue(task_number)


def get_task():
	return task_queue

def get_result():
	return result_queue

# 创建类似的QueueManager
class QueueManager(BaseManager):
	pass


def win_run():
	QueueManager.register('get_task_queue', callable = get_task)
	QueueManager.register('get_result_queue', callable = get_result)

	manager = QueueManager(address = ('127.0.0.1', 5001), authkey = b'abc')

	manager.start()

	try:
		task = manager.get_task_queue()
		result = manager.get_result_queue()

		# 添加任务
		for i in range(10):
			print('put: %d'%i)
			task.put(i)

		print('try get result ...')
		for i in range(10):
			print('result is %s'%result.get())
	except:
		print('Manage error')
	finally:
		manager.shutdown()

if __name__ == "__main__":
	freeze_support()
	win_run()



