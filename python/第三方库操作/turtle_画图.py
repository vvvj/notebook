import turtle as t
from time import sleep

# 五角星
def img1():
    t.goto(0,0)
    for i in range(5):
        t.right(145)
        t.fd(100)
        sleep(3)

def img2():
    t.speed(2)
    t.pensize(1)
    for i in range(100):
        t.fd(3*i)
        t.left(90)
        t.color('red')
    sleep(3)

def img3():
    t.screensize(1000,1000)
    t.goto(0,0)
    t.speed(2)
    t.pensize(1)

    t.left(45)
    t.fd(50)
    
    t.right(135)
    t.fd(20)
    
    t.left(90)
    t.fd(30)

    t.right(90)
    t.fd(20)

    t.right(90)
    t.fd(30)

    t.left(90)
    t.fd(20)

    t.right(135)
    t.fd(50)


    sleep(3)

def img4():
    t.fd(100)
    t.left(90)

    sleep(5)

img3()