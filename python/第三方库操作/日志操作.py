import logging

logging.basicConfig(level=logging.INFO, filename='log.log', filemode='a', format='%(asctime)s - %(filename)s - %(lineno)s - %(name)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)

logger.info('开始打印日志')
logger.debug('xx')
logger.warning('ss')
logger.info('over')

'''
level 日志等级
FATAL          致命错误
CRITICAL  50   特别槽糕的事情，如：内存耗尽、磁盘空间为空
ERROR     40   发生错误
WARNING   30   发生重要的事件，但不是错误
INFO      20   处理请求或者状态变化等日常事务
DEBUG     10   调试过程中使用debug等级，如算法中每个循环的中间状态
NOTEST    00   
'''

'''
format 参数
%(levelno)s : 打印日志级别的数值
%(levelname)s : 打印日志几倍的名称
%(pathname)s : 打印当前执行程序的路径
%(filename)s : 打印当前执行程序名
%(funcName)s : 打印日志的当前函数
%(lineno)d : 打印日志的当前行号
%(asctime)s : 打印日志的时间
%(thread)d : 打印线程ID
%(threadName)s : 打印线程名称
%(process)d : 打印进程id
%(message)s : 打印日志信息 
'''

'''
filename  文件名称
filemode  写入模式，如：w、a
如果只是打印，不需要写入文件，不用设置参数
'''