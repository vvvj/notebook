import time

# 时间戳
shijianchuo = time.time()

# 获取当前时间的时间元祖
localtime = time.localtime()

# 最简单的获取可读的时间模式
localtime = time.asctime()

# 格式化时间
localtime = time.strftime('%Y-%m-%d %H:%M:%S',time.localtime())




import datetime

# date类
# 分别获取后一天，前一天，前七天，前三十天的日期
hou1 = (datetime.date.today() + datetime.timedelta(days = +1)).strftime('%Y-%m-%d')
qian1 = (datetime.date.today() + datetime.timedelta(days = -1)).strftime('%Y-%m-%d')
qian7 = (datetime.date.today() + datetime.timedelta(days = -7)).strftime('%Y-%m-%d')
qian30 = (datetime.date.today() + datetime.timedelta(days = -30)).strftime('%Y-%m-%d')

# datetime类
# 分别打印当前时间，年，月，日
time=datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')
time1=datetime.datetime.now().strftime('%Y')
time2=datetime.datetime.now().strftime('%m')
time3=datetime.datetime.now().strftime('%d')
print(qian1)
