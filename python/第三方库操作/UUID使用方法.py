# UUID 生成唯一ID

# uuid 是Python内置模块，主要有五种算法。
import uuid


# uuid1()    基于时间戳
a1 = uuid.uuid1()
print('uuid1()：', a1)



# uuid2()   基于分布式计算环境DCE（Python中没有这个函数方法）



# uuid3()   基于名字的MD5散列值
a3 = uuid.uuid3(uuid.NAMESPACE_DNS, 'test_hy')
print('uuid3()：', a3)



# uuid4()   基于随机数
a4 = uuid.uuid4() 
print('uuid4()：', a4)




# uuid5()   基于名字的 SHA-1 散列表
a5 = uuid.uuid5(uuid.NAMESPACE_DNS, 'test_hy')
print('uuid5()：', a5)


