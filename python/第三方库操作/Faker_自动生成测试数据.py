# 官方文档：https://faker.readthedocs.io/en/master/index.html
# 安装：pip install Faker
from faker import Faker


# 初始化，设置编码为中文；默认是英文
fake = Faker(locale = 'zh_CN')




# 地址信息类
'''
fake.address()：完整地址，比如：宁夏回族自治区合肥市滨城辽阳街Y座 652690
fake.street_address()：街道+地址，比如：天津路i座
fake.street_name()：街道名，比如：黄路
fake.city_name()：城市名，比如：北京
fake.city()：城市，比如：北京市
fake.province()：省份名，比如：广东省
fake.postcode()：邮编
fake.country()：国家
'''
print('地址信息类'.center(20, '-'))
print('完整地址：', fake.address())
print('街道+地址：', fake.street_address())
print('街道名：', fake.street_name())
print('城市名：', fake.city_name())
print('城市：', fake.city())
print('省份名：', fake.province())
print('邮编：', fake.postcode())
print('国家：', fake.country())





# 公司信息类
'''
fake.company()：公司名，比如：思优科技有限公司
fake.company_suffix()：公司名后缀（公司性质），比如：传媒有限公司
fake.company_prefix()：公司名前缀，比如：戴硕电子
'''
print("公司信息类".center(20, '-'))
print('公司名：', fake.company())
print('公司名后缀：', fake.company_suffix())
print('公司名前缀：', fake.company_prefix())





# 日期类
'''
fake.date(pattern = '%Y-%m-%d', end_datetime = None)
fake.year()：随机年份
fake.day_of_week()：随机星期数
fake.time(pattern = '%H:%M:%S', end_datetime = None)
'''
print("日期类".center(20, '-'))
print('年月日：', fake.date(pattern = '%Y-%m-%d', end_datetime = None))
print('随机年份：', fake.year())
print('随机星期数：', fake.day_of_week())
print('时间：', fake.time(pattern = '%H:%M:%S', end_datetime = None))





# 网络类
'''
fake.company_email()：企业邮箱
fake.email()：邮箱
'''
print("网络类".center(20, '-'))
print('企业邮箱:', fake.company_email())
print("邮箱:", fake.email())





# 个人信息类
'''
fake.name()：姓名
fake.user_name(*args, **kwargs)：用户名，只是随机的英文姓名组合，一把是6位
fake.phone_number()：电话号码
fake.simple_profile(sex=None)：简略个人信息，包括用户名，姓名，性别，地址，邮箱，出生日期。
fake.profile(fields=None, sex=None)：详略个人信息，比简略个人信息多出公司名、血型、工作、位置、域名等等信息。
fake.password()：密码
	参数选项：length：密码长度；special_chars：是否能使用特殊字符；digits：是否包含数字；upper_case：是否包含大写字母；lower_case：是否包含小写字母。
	默认情况：length=10, special_chars=True, digits=True, upper_case=True, lower_case=True
fake.job()：工作
'''
print("个人信息类".center(20, '-'))
print('姓名：', fake.name())
print('电话号码：', fake.phone_number())
print('简略个人信息：', fake.simple_profile(sex = None))
print('详细个人信息：', fake.profile(fields = None, sex = None))
print('密码：', fake.password(length=6))
print('工作：', fake.job())





# 文章类
'''
fake.word(ext_word_list=None)：随机词语
	ext_word_list可以是一个列表，那么词语会从列表中取
fake.words(nb=3, ext_word_list=None)：随机多个词语
	nb是数量，对于words来说是返回多少个词语
fake.sentence(nb_words=6, variable_nb_words=True, ext_word_list=None)：随机短语（会包括短语结束标志点号）
fake.paragraph(nb_sentences=3, variable_nb_sentences=True, ext_word_list=None)：随机段落
fake.paragraphs(nb=3, ext_word_list=None)：多个随机段落
'''
print("文章类".center(20, '-'))
print('随机词语：', fake.word(ext_word_list=['好', '优秀', '差']))
print('随机多个词语：', fake.words(nb=3, ext_word_list=None))
print('随机短语：', fake.sentence(nb_words=6, variable_nb_words=True, ext_word_list=None))
print('随机段落：', fake.paragraph(nb_sentences=3, variable_nb_sentences=True, ext_word_list=None))
print('多个随机段落：', fake.paragraphs(nb=3, ext_word_list=None))




# 数据类型类
'''
fake.pystr(min_chars=None, max_chars=20)：自定义长度的随机字符串
fake.pyint():随机整数
'''
print("数据类型类".center(20, '-'))
print('自定义长度的随机字符串：', fake.pystr(min_chars = None, max_chars = 15))
print('随机整数：', fake.pyint())