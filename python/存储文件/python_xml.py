from xml.dom.minidom import Document
from xml.dom.minidom import parse

# 写 xml 文件
def xml_write():
	data=[{'id':'1','name':'张三'},{'id':'2','name':'李四'}]

	doc=Document()
	root=doc.createElement('message')
	doc.appendChild(root)

	for i in data:
		item=doc.createElement('students')

		id=doc.createElement('id')
		id.appendChild(doc.createTextNode(i['id']))
		name=doc.createElement('name')
		name.appendChild(doc.createTextNode(i['name']))

		item.appendChild(id)
		item.appendChild(name)

		root.appendChild(item)

	f=open('xml.xml','w')
	doc.writexml(f,indent='\t',addindent='\t',newl='\n',encoding='utf-8')
	f.close()



# 解析xml文件
def xml_read():
	pass



