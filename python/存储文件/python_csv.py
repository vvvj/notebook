import csv


# 单行写入
def csv_write1():
	with open('csv.csv','w',newline='')as f:
		w=csv.writer(f)
		w.writerow(['张三', 12, '男'])
		w.writerow(['李四', 13, '女'])
		w.writerow(['王五', 14, '男'])


# 多行写入
def csv_write2():
	data=[['张三', 12, '男'], ['李四', 13, '女'], ['王五', 14, '男']]
	with open('csv.csv', 'w', newline='') as f:
		w=csv.writer(f)
		for i in data:
			w.writerow(i)



# 修改列与列之间的分隔符  delimiter 
def csv_write3():
	data=[['张三', 12, '男'], ['李四', 13, '女'], ['王五', 14, '男']]
	with open('csv.csv', 'w', newline='') as f:
		w=csv.writer(f, delimiter='-')
		for i in data:
			w.writerow(i)



# 使用字典的方式写入
def csv_write4():
    with open('csv.csv','w',newline='') as f:
        fieldnames=['id','name','age']
        data=[{'id':1,'name':'张三','age':11},{'id':2,'name':'李si','age':12}]
        w=csv.DictWriter(f,fieldnames=fieldnames)
        w.writeheader()
        for i in data:
        	w.writerow(i)


# 读取csv文件
def csv_read():
	with open('csv.csv','r')as f:
		r=csv.reader(f)
		for i in r:
			print(i)

