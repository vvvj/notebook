import json


# 将数据写入json文件
def json_write_file():
	data={'name':'张三','age':12}
	with open('json.json','w') as f:
		f.write(json.dumps(data,indent=2, ensure_ascii=False))


# 从json文件中读数据
def json_read_file():
	with open('json.json','r')as f:
		data=json.load(f)
		print(data)

json_write_file()
json_read_file()