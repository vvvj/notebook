# 参考文档：菜鸟教程：https://www.runoob.com/sqlite/sqlite-python.html
#  Python3内置sqlite3数据库，不需要安装，直接导入
import sqlite3


class Test_sql(object):
	# 连接或者创建数据库，创建游标
	con=sqlite3.connect('test.db')
	cur=con.cursor()
	
	# 创建表
	def test_create_table(self):
		self.cur.execute('''
			create table student(
					id int,
					name varchar(20),
					age int
				);
			''')
		print('创建成功')
		self.con.commit() # 提交
		self.con.close()  # 关闭


	# 添加数据
	def test_insert(self):
		self.cur.execute('insert into student(id,name,age) values(1,"张三",11);')
		self.cur.execute('insert into student(id,name,age) values(2,"李四",12);')
		self.cur.execute('insert into student(id,name,age) values(3,"王五",13);')
		self.cur.execute('insert into student(id,name,age) values(4,"赵六",14);')
		self.cur.execute('insert into student(id,name,age) values(5,"孙七",15);')
		self.cur.execute('insert into student(id,name,age) values(6,"犇八",16);')
		self.con.commit()  # 提交
		print('提交成功')
		self.con.close()   # 关闭


	# 查询数据库
	def sql_select(self):
		# 执行sql
		lists=self.cur.execute('select * from student')
		# 打印
		for i in lists:
			print("ID:%s   name:%s   age:%s"%(i[0],i[1],i[2]))
		# 关闭sql连接
		self.con.close()

	# 修改数据
	def test_update(self):
		self.cur.execute('update student set name="犇九" where id=6;')
		self.con.commit()
		print('修改成功')
		self.con.close()


	# 删除数据
	def test_delete(self,id):
		self.cur.execute('delete from student where id="%d";'%id)
		self.con.commit()
		print('删除成功')
		self.con.close()

	# 测试
	def test_aa(self):
		self.cur.execute('select * from student;')
		a=self.cur.fetchall()
		print(a)
		print(len(a))
		print(a[0][1])
		self.con.close()

# Test_sql().test_create_table()
# Test_sql().test_insert()
# Test_sql().test_update()
# Test_sql().sql_select()
# Test_sql().test_delete(6)
# Test_sql().test_aa()