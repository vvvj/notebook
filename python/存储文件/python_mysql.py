import pymysql

class Test_Mysql:
    def __init__(self):
        '''连接mysql数据库'''
        self.conn = pymysql.connect('127.0.0.1', 'root', '123456', 'testdjango', charset='utf8')
        self.cursor = self.conn.cursor()

    def test_select(self):
        '''查询数据'''
        sql = 'select * from student'
        self.cursor.execute(sql)
        # result = self.cursor.fetchone()   # 取一条数据
        result = self.cursor.fetchmany(3)   # 取三条数据
        self.conn.close()
        print(result)

    def test_insert(self):
        '''增加数据'''
        sql = 'insert student values(3, "胡杨", 21)'
        self.cursor.execute(sql)
        self.conn.commit()
        self.conn.close()

    def test_update(self):
        '''修改数据'''
        sql = 'update student set age=18 where id=1'
        self.cursor.execute(sql)
        self.conn.commit()
        self.conn.close()

    def test_delete(self):
        '''删除数据'''
        sql = 'delete from student where id=3'
        self.cursor.execute(sql)
        self.conn.commit()
        self.conn.close()





Test_Mysql().test_select()
# Test_Mysql().test_insert()
# Test_Mysql().test_update()
# Test_Mysql().test_delete()