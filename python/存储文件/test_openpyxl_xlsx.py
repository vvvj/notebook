# 安装
# pip install openpyxl

# 教程文档
# https://openpyxl.readthedocs.io/en/stable/tutorial.html


# 20190624  现在安装是最新版  v2.6.2

# 创建xlsx
def create_xlsx():
	from openpyxl import Workbook
	wb=Workbook()

	ws=wb.active
	ws=wb.create_sheet('abc',0)
	ws['a1']='测试'

	ws1=wb.create_sheet('测试',2)
	id=[i for i in range(1,10)]
	name=[i for i in 'abcdefghij']
	for k,v in zip(id,name):
		ws1['a'+str(k)]=v

	wb.save('a.xlsx')




# 编辑文件
def update_xlsx():
	from openpyxl import load_workbook
	wb = load_workbook('a.xlsx')

	ws=wb['abc']
	ws['d4']='456'
	ws['a10']='12549'

	wb.save('b.xlsx')

# update_xlsx()
# create_xlsx()