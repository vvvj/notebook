"""面向对象--魔术方法"""

"""
__new__(cls)  创建类的对象，优先级大于__init__(self),需要 return

__init__(self) 构造函数：创建类的初始化方法

__del__(self)  析构函数：回收对象


"""



"""比较运算符

__cmp__(self, outher)  可以自由比较

__eq__(self, outher)   判断是否相等

__lt__(self, outher)   判断是否小于

__gt__(self, outher)   判断是否大于

"""


"""数字运算符

__add__(self, outher)   加

__sub__(self, outher)   减

__mul__(self, outher)   乘

__div__(self, outher)   除

"""


"""逻辑运算符

__or__(self, outher)

__and__(self, outher)

"""




class Test():
    def __init__(self, age:int):
        self.age = age

    def __add__(self, outher):
        return self.age + outher.age


a1 = Test(18)
a2 = Test(20)

print(a1 == a2)
print(a1 + a2)



















