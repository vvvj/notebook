from django.shortcuts import render,redirect,HttpResponse

import random
import time
from app.pay import AliPay


# Create your views here.


def index(request):
	return render(request,'index.html')

# 初始化
def get_ali_object():
	app_id='2016092400587135'

	# 异步回调 post
	notify_url='http://127.0.0.1:81/zhifu_return_post/'

	# 同步回调 get
	return_url='http://127.0.0.1:81/zhifu_return_get/'

	# 秘钥
	pri_path='keys/app_private_2048.txt'
	pub_path='keys/alipay_public_2048.txt'

	alipay=AliPay(
		appid=app_id,
		app_notify_url=notify_url,
		return_url=return_url,
		app_private_key_path=pri_path,
		alipay_public_key_path=pub_path,
		debug=True,
		)
	return alipay

def zhifu(request):
	# 商品信息：订单号、商品名称、金额
	order_id='20190110123401'
	shop_title='pdf转Word'
	price='0.01'

	alipay=get_ali_object()

	# 生成url
	params=alipay.direct_pay(
		subject=shop_title,
		out_trade_no=order_id,
		total_amount=price,
		)
	pay_url='https://openapi.alipaydev.com/gateway.do?{0}'.format(params)
	return redirect(pay_url)


# 异步回调 post
def zhifu_return_post(request):
	alipay=get_ali_object()
	if request.method == 'POST':
		from urllib.parse import parse_qs

		body_str=request.body.decode('utf-8')
		post_data=parse_qs(body_str)

		post_dict={}
		for k,v in post_data.items():
			post_dict[k]=v[0]

		sign=post_dict.pop('sign',None)
		status=alipay.verify(post_dict,sign)
		# 获取订单号，修改数据库中订单状态
		out_trade_no=post_dict['out_trade_no']
		return HttpResponse('支付成功,异步回调post')

# 同步回调 get
def zhifu_return_get(request):
	alipay=get_ali_object()
	if request.method == 'GET':
		params=request.GET.dict()
		sign=params.pop('sign',None)
		status=alipay.verify(params,sign)
		# 获取订单号，修改订单状态
		return HttpResponse('支付成功,同步回调get')




	









