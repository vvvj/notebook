import csv
from django.http import HttpResponse


def xiazai_csv(request):
	name = ['张三', '李四', '王五']
	age = [12, 13, 45]
	response = HttpResponse(content_type = 'text/csv')
	response['Content-Disposition'] = 'attachment;filename = csv.csv'

	writer = csv.writer(response)
	for i, j in zip(name, age):
		writer.writerow([i, j])
	return response