# 验证码
# 
# 
# 注意点：
# 1.Windows字体文件
# 2.迁移数据库（session报错）



def cptcha(request):
	# pip install Pillow
	# 引入绘图模块、随机数
	from PIL import Image,ImageDraw,ImageFont
	import random
	# 定义画布背景色，宽高
	bgcolor=(random.randrange(20,100),random.randrange(20,100),random.randrange(20,100))
	width=100
	height=30
	# 创建画布
	im=Image.new('RGB',(width,height),bgcolor)
	# 创建画笔
	draw=ImageDraw.Draw(im)
	# 绘制噪点
	for i in range(0,100):
		xy=(random.randrange(0,width),random.randrange(0,height))
		fill=(random.randrange(0,255),255,random.randrange(0,255))
		draw.point(xy,fill=fill)
	# 验证码备选值
	str='0123456789'
	# 定义四位数验证码
	rand_str=''
	for i in range(0,4):
		rand_str+=str[random.randrange(0,len(str))]
	# 构建字体对象
	font=ImageFont.truetype(r'C:\Windows\Fonts\Ebrima.ttf',18)
	# 构建字体颜色
	fontcolor1=(255,random.randrange(0,255),random.randrange(0,255))
	fontcolor2=(255,random.randrange(0,255),random.randrange(0,255))
	fontcolor3=(255,random.randrange(0,255),random.randrange(0,255))
	fontcolor4=(255,random.randrange(0,255),random.randrange(0,255))
	# 绘制4个文字
	draw.text((5,2),rand_str[0],font=font,fill=fontcolor1)
	draw.text((25,2),rand_str[1],font=font,fill=fontcolor2)
	draw.text((50,2),rand_str[2],font=font,fill=fontcolor3)
	draw.text((75,2),rand_str[3],font=font,fill=fontcolor4)
	# 释放画笔
	del draw
	# 内存文件操作
	import io 
	buf=io.BytesIO()
	im.save(buf,'png')
	response=HttpResponse(buf.getvalue(),'image/png')
	# 存入session
	request.session['cptcha_value']=rand_str

	return response