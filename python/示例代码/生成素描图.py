import cv2

# 读取图片
img_rgb = cv2.imread('1.jpg')

# 灰度化
img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_RGB2GRAY)

# 模糊化：中值滤波函数
# img_gray = cv2.medianBlur(img_gray, 5)

# 模糊化：高斯滤波
img_blur = cv2.GaussianBlur(img_gray, ksize = (21, 21), sigmaX = 0, sigmaY = 0)

# 二值化
# img_edge = cv2.adaptiveThreshold(img_gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, blockSize = 3, C = 2)

# 原图和模糊图融合
img_edge = cv2.divide(img_gray, img_blur, scale = 255)

# 保存图片
cv2.imwrite('d.jpg', img_edge)

