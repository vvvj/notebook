# https://sz.lianjia.com/ershoufang/pg1/
# 总价、户型、楼层、建筑面积、结构、套内面积、朝向、建筑结构、装修情况、梯户比例、配备电梯、产权年限
# 共100页，每页30条，共3k数据

import requests
from bs4 import BeautifulSoup 


# headers信息
header={
    'User-Agent':'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36',
    'Cookie':'select_city=440300; lianjia_uuid=e33dc957-4070-4a49-a371-d2c50544255e; __guid=230593118.1210199194345211000.1551867604811.3164; UM_distinctid=169528472901e2-063a7efc72e808-454c092b-15f900-16952847291115; _smt_uid=5c7f9ed5.a141f8f; _jzqckmp=1; _ga=GA1.2.924132415.1551867608; _gid=GA1.2.467728510.1551867608; all-lj=762328e22710c88ff41f391dedabbc6f; lianjia_ssid=f7ebbc7b-73c9-4f40-973a-c7de8c980f03; TY_SESSION_ID=d7cfb43d-f234-4155-a7ae-056333c5135e; Hm_lvt_9152f8221cb6243a53c83b956842be8a=1551867606,1551945576; CNZZDATA1255849469=1869204304-1551867239-https%253A%252F%252Fwww.baidu.com%252F%7C1551944092; CNZZDATA1254525948=2082054674-1551863892-https%253A%252F%252Fwww.baidu.com%252F%7C1551942338; CNZZDATA1255633284=1544720588-1551864023-https%253A%252F%252Fwww.baidu.com%252F%7C1551943294; CNZZDATA1255604082=940745501-1551863531-https%253A%252F%252Fwww.baidu.com%252F%7C1551941754; _jzqa=1.3521991919745810000.1551867606.1551867606.1551945577.2; _jzqc=1; _jzqy=1.1551867606.1551945577.1.jzqsr=baidu|jzqct=%E9%93%BE%E5%AE%B6.-; _qzjc=1; monitor_count=9; Hm_lpvt_9152f8221cb6243a53c83b956842be8a=1551946054; _qzja=1.2146304095.1551867605972.1551867605973.1551945576959.1551945745647.1551946055562.0.0.0.9.2; _qzjb=1.1551945576958.4.0.0.0; _qzjto=4.1.0; _jzqb=1.4.10.1551945577.1; _gat=1; _gat_global=1; _gat_new_global=1; _gat_dianpu_agent=1'
    }


# 详细页面解析
def jiexi(url):
    res=requests.get(url,headers=header)
    soup=BeautifulSoup(res.text,'lxml')

    price=''.join([i.text for i in soup.find_all('span',class_='total')])
    f=open('链家二手房信息.txt','a')
    f.write('\n\n房屋总价 ：'+ price+'万\n')

    html="".join([ str(i.ul) for i in soup.find_all('div',class_='base')])#[4:-5]
    
    soup2=BeautifulSoup(html,'lxml')
    for i in soup2.find_all('li'):
        f.write(i.span.text+' : '+i.text+'\n')
    f.write('\n')
    f.write('+'*30)
    f.close()



# 解析出详细页面网址
def lianjia_ershoufang(url):
    res=requests.get(url,headers=header)

    soup=BeautifulSoup(res.text,'lxml')
    num=0
    for i in soup.find_all('div',class_='title'):
        url2=i.a['href']
        jiexi(url2)
        
        num+=1
        if num ==30:
            break



def urls():
    for i in range(1,101):
        url='https://sz.lianjia.com/ershoufang/pg%s/'%i
        print('正在存储第'+str(i)+'页')
        lianjia_ershoufang(url)


urls()


