'''
pip install requests
'''
# 邮件
import smtplib
from email.mime.text import MIMEText

# 爱词霸
import requests
import json

# 定时
from time import sleep


# 爬虫：爬取爱词霸的每日一句
class Pa_Iciba(object):
    def __init__(self):
        self.iciba()

    def iciba(self):
        url='http://open.iciba.com/dsapi/'
        res=requests.get(url)
        text_json=json.loads(res.text)
        print(text_json)
        # 打印需要的内容
        print(text_json['content']) # 英文
        print(text_json['note'])    # 中文
        print(text_json['translation']) # 小编
        print(text_json['tts'])     # 音频
        print(text_json['picture2']) # 大图
        print(text_json['fenxiang_img'])  # 分享图
        





# 爬虫_邮件：爬取iciba的每日一句，并每分钟发送一次邮件
class Time_Email_Iciba(object):
    def __init__(self):
        self.timing_send_email()

    # 爬数据
    def iciba(self):
        url="http://open.iciba.com/dsapi"
        res=requests.get(url)
        text_json=json.loads(res.text)
        
        title=text_json['caption']
        content=text_json['note']+'\n\n'+text_json['content']+'\n\n'+text_json['translation']+'\n\n'+text_json['dateline']
        return title,content

    # 发送邮件方法
    def send_email(self, title, content):
        print('正在发送邮件...')

        #---参数
        email_url='13211180565@163.com'
        pwd='huyang123xxx'

        #---内容
        title=title
        content=content
        msg=MIMEText(content,'html','utf-8')
        msg['form']=email_url
        msg['to']=email_url
        msg['subject']=title

        #---发送
        smtp=smtplib.SMTP()
        smtp.connect('smtp.163.com')
        smtp.login(email_url,pwd)
        smtp.sendmail(email_url,email_url,msg.as_string())
        smtp.quit()
        
        print('发送成功!')

    # 定时发送
    def timing_send_email(self):
        num=0
        while True:
            title,content=self.iciba()
            self.send_email(title,content)
            num+=1
            print('第'+str(num)+'次发送')
            sleep(60)



