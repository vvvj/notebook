from PIL import Image
import os

def get_size(file_path):
    '''获取文件大小'''
    size = os.path.getsize(file_path)
    return size / 1024



def yasuo_image(infile, outfile, mb = 150, step = 10, quality = 80):
    '''压缩图片'''
    o_size = get_size(infile)
    
    while o_size > mb:
        im = Image.open(infile)
        im.save(outfile, quality = quality)
        if quality - step < 0:
            break
        quality -= step
        o_size = get_size(outfile)

    return outfile, get_size(outfile)


yasuo_image('1.jpg', '2.jpg')