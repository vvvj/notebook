'''itertools 篇
'''
# enumerate  在循环的时候打印下标
def test1():
	names = ['张三', '李四', '王五']
	for i, name in enumerate(names):
		print(i, name)


# product 扁平化多层嵌套循环
def test2():
	from itertools import product
	list1 = [4, 5, 6]
	list2 = [3, 5, 6]
	list3 = [1, 2, 3]
	for x, y, z in product(list1, list2, list3):
		if x + y + z == 12:
			print(x, y, z)


'''定时器
pip install schedule
'''
def test3():
	import schedule

	def xx():
		print('xxaa')
	
	schedule.every().day.at('10:00').do(xx)

	while True:
		schedule.run_pending()


# 实现进度条
def test4(percent = 0, width = 30):
	left = width * percent // 100
	right = width - left
	print('\r[', '#' * left, ' ' * right, ']', f'{percent : .0f}%', sep = '', end = '', flush = True)



# 优雅的打印嵌套类型的数据
def test5():
	import json
	dict_data = {'name':'张三', 'age':12}
	print(json.dumps(dict_data, indent = 4, sort_keys = True))

	import pprint
	list_dict_data = [{'name':'zhangsn', 'age':12}, {'name':'lisi', 'gae':11}]
	pprint.pprint(list_dict_data, width = 4)



