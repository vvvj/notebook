import imageio
import os

class Create_Gif(object):
	def __init__(self):
		# self.imgs = ['img/1.jpg', 'img/2.jpg', 'img/3.jpg']
		self.imgs = [ 'img/' + i for i in os.listdir('img')]
		self.gif_name = 'new.gif'
		self.duration = 0.1
		self.duo_img_to_gif()

	def duo_img_to_gif(self):
		frams = []
		for img in self.imgs:
			frams.append(imageio.imread(img))
		imageio.mimsave(self.gif_name, frams, 'GIF', duration=self.duration)
		print('ok')

Create_Gif()

