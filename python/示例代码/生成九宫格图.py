from PIL import Image
import uuid, time

start = time.time()

image = Image.open('a.jpg')
width, height = image.size

# 高和宽进行比较，较大的为新图片的长宽
new_length = height if height > width else width

# 创建一张正方形空图片，底色为白色
new_image = Image.new(image.mode, (new_length, new_length), color = 'white')

# 将要处理的图片粘贴到新创建的图片上，居中
# 如果高度大于宽，则填充图片的宽
if height > width:
	new_image.paste(image, int((new_length - width) / 2), 0)
else:
	new_image.paste(image, (0, int((new_length - height) / 2)))


# 朋友圈一排三张图片因此宽度切割成3分
new_length = int(new_length / 3)

# 用于保存每一份切图
box_list = []
for i in range(0, 3):
	for j in range(0, 3):
		box = (j * new_length, i * new_length, (j + 1) * new_length, (i + 1) * new_length)
		box_list.append(box)

# 对图片进行切割
image_list = [new_image.crop(box) for box in box_list]

# 保存图片
data = dict()
num = 1
for i in image_list:
	name = str(uuid.uuid4()) + '.jpg'
	data['img' + str(num)] = name
	i.save('a/' + name)
	num += 1


end = time.time()

print('ok', end-start)
print(data)








