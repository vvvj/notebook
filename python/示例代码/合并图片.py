import cv2
import numpy as np 


# 读取图片
img1 = cv2.imread(r'C:/a.jpg')
img2 = cv2.imread(r'C:/xx.jpg')


# 合并图片
# image = np.concatenate((img1, img2))         # 纵向
image = np.concatenate([img1, img2], axis=1)   # 横向

# 显示图片
# cv2.imshow('image', image)
# cv2.waitKey(0)
# cv2.destroyAllWindows()

# 保存图片
cv2.imwrite('return.jpg', image)   