'''
title: PDF拆分
author: 十一
datatime: 20191120 165300
explain: 将单个PDF按页拆分成多个PDF文件
'''

import os
from PyPDF2 import PdfFileReader, PdfFileWriter

def pdf_split(file_name, out_dir):
    input_pdf = PdfFileReader(file_name, 'rb')
    file_page_number = input_pdf.getNumPages()

    count = 0
    for index in range(file_page_number):
        count += 1
        out_put = PdfFileWriter()
        out_put.addPage(input_pdf.getPage(index))
        out_file_name = '00'+ str(count) + '.pdf'

        # 生成PDF
        out_put_str = open(os.path.join(out_dir, out_file_name), 'wb')
        out_put.write(out_put_str)
        out_put_str.close()
        




# file_name = 'opencv.pdf'
# out_dir = 'a'
# pdf_split(file_name, out_dir)
# print('over')