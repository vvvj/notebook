import time
import queue

class Mianxiangduixiang(object):

    def __init__(self):
        self.test3()

    def test(self):
        print('abc')

    def test1(self):
        print('def')

    def test2(self):
        start=time.ctime()
        for i in range(2001):
            print(i)
        end=time.ctime()
        print(start)
        print(end)

    def test3(self):
        start=time.ctime()
        q=queue.Queue()  # 先进先出
        for i in range(2001):
            q.put(i)
        while not q.empty():
            print(q.get())
        end=time.ctime()
        print(start)
        print(end)

    def test4(self):
        start=time.ctime()
        q=queue.LifoQueue()  # 后进先出
        for i in range(2001):
            q.put(i)
        while not q.empty():
            print(q.get())
        end=time.ctime()
        print(start)
        print(end)


if __name__ == '__main__':
    Mianxiangduixiang()
    
