'''
title: 合并PDF
author: 十一
datatime: 20191120 161200
explain: 将整个文件夹的PDF合并成一个PDF
@file_dir_path：需要合并的文件夹路径
@out_file_name：合并后文件的名称
'''

import os
from PyPDF2 import PdfFileReader, PdfFileWriter


def pdf_merger(file_dir_path, out_file_name):
    file_list = os.listdir(file_dir_path)
    out_put = PdfFileWriter()
    out_put_pages = 0

    for file_page in file_list:
        page_file_path = os.path.join(file_dir_path, file_page)
        input_pdf = PdfFileReader(page_file_path, 'rb')

        # 计算单PDF文件中总页数
        page_number = input_pdf.getNumPages()
        out_put_pages += page_number

        # 分别将 page 添加到 output 中
        for index in range(page_number):
            out_put.addPage(input_pdf.getPage(index))
    
    out_put_str = open(out_file_name, 'wb')
    out_put.write(out_put_str)
    out_put_str.close()




# file_dir_path = 'a'
# out_file_name = 'xx.pdf'
# pdf_merger(file_dir_path, out_file_name)
# print('over')
