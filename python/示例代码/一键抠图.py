'''
一键抠图：https://www.remove.bg/usage
免费账户每月50张图片
'''

import requests


image_path = '1.jpg'

response = requests.post(
	'https://api.remove.bg/v1.0/removebg',
	files =  {'image_file': open(image_path, 'rb')},
	data = {'size': 'auto'},
	headers = {'X-Api-Key': 'KJzSz7k46jXSn59DoWFtrq62 '}
	)
if response.status_code == requests.codes.ok:
	with open('res.png', 'wb') as out:
		out.write(response.content)
else:
	print('Error:', response.status_code, response.text)