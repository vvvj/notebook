import pandas as pd 
import re

d = {
    "customer": ['a', 'b', 'c', 'd'],
    "sales": [1100, '950.6RMB', '$400', '$1250.71']
}

# 将数据转为矩阵
df = pd.DataFrame(d)
print(df)
print(df['sales'].apply(type))  # 打印 sales 列的值得数据类型

# 清洗数据，将 sales 列转为浮点数
df['sales'] = df['sales'].replace('[$, RMB]', '', regex=True).astype('float')

print(df)