import pandas as pd 

d = {
    "gender": ['male', 'frmale', 'male', 'frmale'],
    "color": ['red', 'yellow', 'blue', 'green'],
    "age": [25, 23, 35, 30]
}

df = pd.DataFrame(d)
print(df)

d = {"male": 0, "frmale": 1}
df['gender2'] = df['gender'].map(d)
print(df)
