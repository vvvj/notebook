import pandas as pd 

d = {
    'name': ['张三', '李四', '王五'],
    'age': [11, 12, 13],
    'sex': [0, 1, 0]
}

df = pd.DataFrame(d)
print(df)


meta = df.pop('sex').to_frame()
print(meta)
print(df)
