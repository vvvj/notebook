'''
对数据进行统计，找出频次较低的值，替换为 None
'''
import pandas as pd 

d = {
    'name': ['a1', 'a2', 'a3', 'a4', 'a5', 'a6', 'a7', 'a8', 'a9', 'a10', 'a11', 'a12'],
    'res': ['a', 'b', 'c', 'd', 'a', 'a', 'a', 'b', 'b', 'a', 'a', 'a']
}

df = pd.DataFrame(d)

# 统计频次，并归一
c = df['res'].value_counts(normalize=True)
print(c)


# 设计阈值，过滤频次少的值
p = 0.1
small_res = c[c < p].index
print(small_res)


# 替换值
df['res'] = df['res'].replace(small_res, 'None')
print(df)