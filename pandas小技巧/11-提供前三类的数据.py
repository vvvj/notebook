import pandas as pd 

# 读取 Excel
df = pd.read_excel('a.xlsx')
print(df)

# 统计词频
vc = df['name'].value_counts()
print(vc)

# 筛选出 top2 的index
top2 = vc[0:2].index
print(top2)

# 使用得到的 top2 的index 结合 isin 选择出相应的 df
df_top = df[df['name'].isin(top2)]
print(df_top)



