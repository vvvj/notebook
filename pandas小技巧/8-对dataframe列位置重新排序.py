import pandas as pd 
import numpy as np 

# 构建数据
df = pd.DataFrame(np.random.randint(0, 20, size=(5, 7)), columns=list('abcdefg'))
print(df)


# 修改顺序
df2 = df[['a', 'c', 'b', 'd', 'g', 'f', 'e']]
print(df2)