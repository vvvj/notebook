import pandas as pd 
import xlsxwriter
import xlrd


data = pd.read_excel(r'a.xlsx', encoding='gbk')
area_list = list(set(data['地区']))
writer = pd.ExcelWriter(r'b.xlsx', engine='xlsxwriter')
data.to_excel(writer, sheet_name='总表', index=False)


# 遍历，拆分成多个 sheet
'''
for j in area_list:
	df = data[data['地区'] == j]
	df.to_excel(writer, sheet_name=j, index=False)
writer.save()

'''

# 遍历拆分成多个表
for j in area_list:
	df = data[data['地区'] == j]
	writer = pd.ExcelWriter('%s.xlsx'%j, engine='xlsxwriter')
	df.to_excel(writer, sheet_name=j, index=False)
	writer.save()
