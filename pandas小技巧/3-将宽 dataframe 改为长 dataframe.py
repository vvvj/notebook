import pandas as pd 

d = {
    "c": ['yuwen', 'shuxue', 'yingyu'],
    "zhangsan": [89, 20, 60],
    "lisi": [50, 40, 58],
    "wangwu": [30, 60, 70]
}

# 将数据转为矩阵
df = pd.DataFrame(d)
print(df)

# 将宽 dataframe 改为长 dataframe
df = df.melt(id_vars='c', var_name='name', value_name='score')
print(df)