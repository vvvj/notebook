import pandas as pd 
import numpy as np 

# 构建数据
df = pd.DataFrame(np.random.randint(1, 10, size=(240, 3)), columns=['编码', '销量', '库存'])
print(df)

# index 修改成 以小时为步长的数据
df.index = pd.util.testing.makeDateIndex(240, freq='H')
print(df)
