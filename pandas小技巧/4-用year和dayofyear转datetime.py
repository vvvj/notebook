import pandas as pd 
d = {
    "year": [2020, 2019, 2018],
    "day_of_year": [20, 60, 1]
}

df = pd.DataFrame(d)
print(df)

# 创建整数
df['int_number'] = df['year']*1000 + df['day_of_year']
print(df)

# 将创建的整数转为 date
df['date'] = pd.to_datetime(df['int_number'], format='%Y%j')
print(df)
