# -*- coding:utf-8 -*- 
import pyautogui
from time import sleep
import time
import smtplib
from email.mime.text import MIMEText
import psutil

'''
书籍高拍仪 v2.3.5
针对处理模式做疲劳测试
1.单页展平
2.双页展平
3.不展平
4.纠偏

操作步骤：
1.输入需要测试的处理模式
2.点击拍摄

ps:
每次拍摄单页后等待 10s
每次拍摄双页后等待 15s
不展平和纠偏等待  7s

在程序运行时，发送邮件
在程序停止运行时，发送邮件
每拍摄一次，打印拍摄次数及时间
'''

class Fatigue_test(object):
    def __init__(self):
        # pyautogui 框架设置
        pyautogui.PAUSE=1
        pyautogui.FAILSAFE=True

        # 获取当前时间
        now_time=time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())

        # 开始运行
        num=input('单页模式选择：1\n双页模式选择：2\n不展平选择：3\n纠偏选择：4\n')
        self.start_send_email(now_time,num)  # 开始运行时，发送邮件
        self.chulimoshi(num)   # 操作软件的处理模式
        self.paizhao(num)   # 开始拍照

    # 选择处理模式
    def chulimoshi(self,num):
        if num == '1':
            left,top,width,height=pyautogui.locateOnScreen('1.jpg')
            x,y=pyautogui.center((left,top,width,height))
            pyautogui.click(x,y,left)
        elif num == '2':
            left,top,width,height=pyautogui.locateOnScreen('2.jpg')
            x,y=pyautogui.center((left,top,width,height))
            pyautogui.click(x,y,left)
        elif num == '3':
            left,top,width,height=pyautogui.locateOnScreen('3.jpg')
            x,y=pyautogui.center((left,top,width,height))
            pyautogui.click(x,y,left)
        else:
            left,top,width,height=pyautogui.locateOnScreen('3.jpg')
            x,y=pyautogui.center((left,top,width,height))
            pyautogui.click(x,y,left)
            left,top,width,height=pyautogui.locateOnScreen('4.jpg')
            x,y=pyautogui.center((left,top,width,height))
            pyautogui.click(x,y,left)
            pyautogui.doubleClick()

    # 拍照
    def paizhao(self,num):
        count=1
        try:
            while True:
                try:
                    left,top,width,height=pyautogui.locateOnScreen('0.jpg')
                    x,y=pyautogui.center((left,top,width,height))
                    pyautogui.click(x,y,left)
                except:
                    left,top,width,height=pyautogui.locateOnScreen('5.jpg')
                    x,y=pyautogui.center((left,top,width,height))
                    pyautogui.click(x,y,left)
                if num == '1':
                    print('正在拍摄单页展平图片，第%d次'%count)                
                    sleep(10)
                elif num == '2':
                    print('正在拍摄双页展平图片，第%d次'%count)
                    sleep(15)
                elif num == '3':
                    print('正在拍摄不展平图片，第%d次'%count)
                    sleep(7)
                else:
                    print('正在拍摄纠偏图片，第%d次'%count)
                    sleep(7)
                count+=1
        except:
            now_time=time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
            self.end_send_email(now_time,num,count)



    # 运行开始时，发送邮件
    def start_send_email(self,now_time,num):
        # 参数
        email_url='13211180565@163.com'
        pwd='huyang123'

        # 内容
        title='书籍高拍仪 v2.3.5 疲劳测试开始'
        content='<b>书籍高拍仪 v2.3.5 自动化疲劳测试</b>'
        if num == '1':
            content+='<p>单页模式</p>'
        elif num == '2':
            content+='<p>双页模式</p>'
        elif num == '3':
            content+='<p>不展平模式</p>'
        else:
            content+='<p>纠偏模式</p>'
        content+='%s<p>%s</p>'%(self.psutil_rss(),now_time)
        msg=MIMEText(content,'html','utf-8')
        msg['form']=email_url
        msg['to']=email_url
        msg['subject']=title

        # 发送
        smtp=smtplib.SMTP()
        smtp.connect('smtp.163.com')
        smtp.login(email_url,pwd)
        smtp.sendmail(email_url,email_url,msg.as_string())
        smtp.quit()
        
    
    # 软件崩溃时，发送邮件
    def end_send_email(self,now_time,num,count):
        # 参数
        email_url='13211180565@163.com'
        pwd='huyang123'

        # 内容
        title='软件崩溃'
        content='<b>书籍高拍仪 v2.3.5 自动化疲劳测试</b>'
        if num == '1':
            content+='<p>单页模式</p>'
        elif num == '2':
            content+='<p>双页模式</p>'
        elif num == '3':
            content+='<p>不展平模式</p>'
        else:
            content+='<p>纠偏模式</p>'
        content+='<p>%d</p>%s<p>%s</p>'%(count,self.psutil_rss(),now_time)
        msg=MIMEText(content,'html','utf-8')
        msg['form']=email_url
        msg['to']=email_url
        msg['subject']=title

        # 发送
        smtp=smtplib.SMTP()
        smtp.connect('smtp.163.com')
        smtp.login(email_url,pwd)
        smtp.sendmail(email_url,email_url,msg.as_string())
        smtp.quit()


    # 查看内存使用情况
    def psutil_rss(self):
        pids=psutil.pids()
        pid=''.join([str(i) for i in pids if psutil.Process(i).name() == 'Eloam Book Scanner.exe'])
        p=psutil.Process(int(pid))
        pname=p.name()
        p_neicun=p.memory_info().rss
        text='<p>进程ID:%s<br/>进程名：%s<br/>内存使用情况：%d</p>'%(pid,pname,p_neicun)
        return text 

Fatigue_test()
























    

