# -*- coding:utf-8 -*- 
import pyautogui,time
from time import sleep


# 基本配置
pyautogui.PAUSE=1
pyautogui.FAILSAFE=True



# 开始
now_time=time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
print('开始时间：',now_time)

# 拍照50张
num=1
while num<50:
	# 拍照
	try:
		left,top,width,height=pyautogui.locateOnScreen('0.jpg')
		x,y=pyautogui.center((left,top,width,height))
		pyautogui.click(x,y,left)
	except:
		left,top,width,height=pyautogui.locateOnScreen('5.jpg')
		x,y=pyautogui.center((left,top,width,height))
		pyautogui.click(x,y,left)
	# 记录
	now_time=time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
	print('第%d次:'%num,now_time)
	sleep(10)
	num+=1


now_time=time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
print('结束时间：',now_time)