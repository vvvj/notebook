from selenium import webdriver
from time import sleep

'''
表单对象的操作比较简单，只需要记住下面几点
使用send_keys方法往多行文本框和单行文本框赋值；
使用click方法选择checkbox
使用click方法选择radio
使用click方法点击button
使用click方法选择option，从而达到选中select下拉框中某个具体菜单项的效果
'''

dr=webdriver.Chrome()

url="file:///F:/PYTHON/自动化/处理表单元素/from.html"
dr.get(url)

# 点击复选框
sleep(1)
dr.find_element_by_css_selector("input[type='checkbox']").click()

# 点击单选框
sleep(1)
dr.find_element_by_css_selector('input[type="radio"]').click()

# 点击下拉框
sleep(1)
dr.find_element_by_tag_name('select').find_elements_by_tag_name('option')[1].click()

# 点击提交
sleep(1)
dr.find_element_by_css_selector('input[type=submit]').click()












