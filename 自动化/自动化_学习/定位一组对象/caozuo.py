from selenium import webdriver
import time

# 打开浏览器
dr=webdriver.Chrome()


# 打开url
url="file:///F:/PYTHON/自动化/定位一组对象/checkbox.html"
dr.get(url)

# 打印url标题
print('标题:%s'%(dr.title))




# 把所有复选框选上  *注意 elements  括号里用双引号
checkboxes=dr.find_elements_by_css_selector("input[type=checkbox]")
for i in checkboxes:
	i.click()
time.sleep(2)
dr.refresh()



# 打印复选框个数
print(len(checkboxes))




# 选择页面上所有的input，然后从中过滤出所有的复选并勾选
inputs=dr.find_elements_by_tag_name("input")
for i in inputs:
	if i.get_attribute("type")=="checkbox":
		i.click()
time.sleep(2)
dr.refresh()


# 选择最后一个复选框勾选上
dr.find_element_by_id("c3").click()


# 关闭
print("5秒后关闭浏览器")
time.sleep(5)
dr.quit()