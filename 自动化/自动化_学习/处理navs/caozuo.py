from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

dr=webdriver.Chrome()

url="file:///F:/PYTHON/自动化/%20处理navs/breadcrumb.html"
dr.get(url)


# 方法1：层级定位，先定位ul再定位li
dr.find_element_by_class_name('nav').find_element_by_link_text('About').click()
time.sleep(1)

# 方法2: 直接定位link
dr.find_element_by_link_text('Home').click()
time.sleep(1)

dr.quit()