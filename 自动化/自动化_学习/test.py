from selenium import webdriver
import time

# 打开浏览器。此处是谷歌
dr=webdriver.Chrome()


# 打开url
dr.get('http://www.baidu.com')


# 打印url标题
print("url标题为：%s"%(dr.title))


# 打印url
print('url为：%s'%(dr.current_url))




# 最大化浏览器
print('两秒后最大化浏览器')
time.sleep(2)
dr.maximize_window()



# 设置浏览器大小
print('两秒后设置浏览器大小')
time.sleep(2)
dr.set_window_size(240,320)



# 关闭浏览器：quit（）、close（）两种关闭方式
print("准备关闭(5秒)")
time.sleep(5)
dr.quit()
print('已关闭')