from selenium import webdriver
from time import sleep


# 打开浏览器
dr=webdriver.Chrome()


# 打开网页
url="https://jinshuju.net/f/kRXoEv"
dr.get(url)



# 评价质量-4颗星
sleep(2)
dr.find_element_by_class_name("type-star").find_elements_by_class_name("rating-icon-star")[3].click()



# 复选框，不选性能测试
sleep(3)
checkboxs=dr.find_elements_by_class_name("selected-icon")
for i in checkboxs:
	if i == checkboxs[1]:
		continue
	i.click()
	sleep(1)


# 填写建议
sleep(2)
dr.find_element_by_id("entry_field_3").send_keys("平台非常好，希望可以多给一些项目实战")


# 点击提交按钮
sleep(2)
dr.find_element_by_name("commit").click()


# 关闭
sleep(3)
dr.quit()

