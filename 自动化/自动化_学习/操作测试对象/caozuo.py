from selenium import webdriver
import time



# click 点击对象
# send_keys 在对象上模拟按键输入
# clear 清除对象的内容



# 打开浏览器
dr=webdriver.Chrome()


# 打开url
url="http://www.baidu.com"
dr.get(url)

# 输出url标题
print("标题：",dr.title)


# 在文本框中输入 需要查询的内容
time.sleep(2)
dr.find_element_by_id("kw").send_keys("深圳天气")

# 点击查询
time.sleep(2)
dr.find_element_by_id("su").click()



# 关闭浏览器
print("5秒后关闭浏览器")
time.sleep(5)
dr.quit()

