from selenium import webdriver
import time


# 打开浏览器
dr=webdriver.Chrome()


# 打开url
url='file:///F:/PYTHON/自动化/简单的对象定位/test.html'
dr.get(url)





# 定位
# name
dr.find_element_by_name('email').click()

# id
dr.find_element_by_id('inputEmail').click()

# tag_name
tagname=dr.find_element_by_tag_name('form').get_attribute('class')
print(tagname)

# class_name
dr.find_element_by_class_name("controls").click()

# link_text
dr.find_element_by_link_text('register').click()

# partial_link_text
dr.find_element_by_partial_link_text('reg').click()

# css_selector
dr.find_element_by_css_selector('.controls').click()

# xpath
dr.find_element_by_xpath('/html/body/form/div[3]/div/label/input').click()





# 关闭
print('5秒后关闭')
time.sleep(5)
dr.quit()