from selenium import webdriver
import time 


'''
在web自动化测试中，我们需要获取测试对象的四种状态
是否显示。使用element.is_displayed()方法；
是否存在。使用find_element_by_xxx方法，捕获其抛出的异常, 如果存在异常的话则可以确定该元素不存在；
是否被选中。一般是判断表单元素，比如radio或checkbox是否被选中。使用element.is_selected()方法；
是否enable，也就是是否是灰化状态。使用element.is_enabled()方法；
'''

dr=webdriver.Chrome()

url="file:///F:/PYTHON/%E8%87%AA%E5%8A%A8%E5%8C%96/%E8%8E%B7%E5%8F%96%E6%B5%8B%E8%AF%95%E5%AF%B9%E8%B1%A1%E7%9A%84%E7%8A%B6%E6%80%81/status.html"
dr.get(url)


h3=dr.find_element_by_tag_name("h3")
bool_text=h3.is_displayed()
bool_text2=h3.is_enabled()
print(bool_text)
print(bool_text2)


# 判断单选是否选择
radio=dr.find_element_by_name("radio")
# radio.click()
zhuangtai=radio.is_selected()
print("单选框状态",zhuangtai)


# 判断元素是否存在
try:
    dr.find_element_by_id('none')
except: 
    print('element does not exist')





