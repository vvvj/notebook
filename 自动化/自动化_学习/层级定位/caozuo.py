from selenium import webdriver
from selenium.webdriver.support.ui import  WebDriverWait
import time

# 打开浏览器
dr=webdriver.Chrome()


# 打开url
url="file:///F:/PYTHON/自动化/层级定位/level_locate.html"
dr.get(url)


# 打印url标题
print("标题：%s"%(dr.title))


dr.find_element_by_link_text('Link1').click()

WebDriverWait(dr, 10).until(lambda the_driver: the_driver.find_element_by_id('dropdown1').is_displayed())
menu = dr.find_element_by_id('dropdown1').find_element_by_link_text('Another action')

webdriver.ActionChains(dr).move_to_element(menu).perform()



# 关闭浏览器
print("5秒后关闭浏览器")
time.sleep(5)
dr.quit()
print("已关闭")
