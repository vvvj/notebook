from selenium import webdriver
from time import sleep
from lxml import etree
from sql import AllCodeSql
from jsonFile import JsonFile


class AllCode(object):
    '''爬取邮编'''

    root = webdriver.Chrome()
    
    # 获取省url
    def provinceUrl(self):
        self.root.get('http://alexa.ip138.com/post/anhui/anqing/')
        data = []
        url_list = self.root.find_elements_by_xpath('/html/body/table[4]/tbody/tr[1]/td/a')
        for i in url_list[:-3]:
            data.append(i.get_attribute('href'))
        
        # 存储到json文件
        JsonFile().jsonWriteFile('provinceUrl.json', data)
        self.root.quit()
        

    # 获取每个省对应的市的url
    def provinceToCity(self):
        data = []
        provinceURL = JsonFile().jsonReadFile('provinceUrl.json')
        for i in provinceURL:
            self.root.get(i)
            data.append(self.root.find_element_by_xpath('/html/body/table[4]/tbody/tr[2]/td[1]/a').get_attribute('href')) 
        self.root.quit()

        # 存储到json文件
        JsonFile().jsonWriteFile('cityUrl.json', data)


    # 爬取所有市的url
    def allCity(self):
        citys = JsonFile().jsonReadFile('cityUrl.json')
        data = []

        # 1.获取各省 所有市的url
        for i in citys:
            self.root.get(i)
            citys = self.root.find_elements_by_xpath('/html/body/table[4]/tbody/tr[2]/td/a')
            for city in citys:
                data.append(city.get_attribute('href'))

        self.root.quit()

        # 存储到json文件
        JsonFile().jsonWriteFile('citys.json', data)


    # 获取每一个城市的数据，并保存数据
    def saveCode(self):
        cityURL = JsonFile().jsonReadFile('citys.json')
        num = 0

        # id, code, province, city, area, address
        # 解析出 省，市，页数
        for url in cityURL:
            self.root.get(url)
            province = self.root.find_element_by_xpath('/html/body/table[6]/tbody/tr[3]/td[1]/a').text   # 省
            city = self.root.find_element_by_xpath('/html/body/table[6]/tbody/tr[3]/td[2]/a').text       # 市
            pages = self.root.find_element_by_xpath('/html/body/table[5]/tbody/tr/td[2]/span').text      # 页数
            pages = int(pages[2:])

            print(f'{city}----{pages}')

            # 解析每一页的数据
            for page in range(1, pages+1):
                newURL = url

                print(page, end='---')
                # 每三页停1秒
                if page % 3 == 0:
                    sleep(1)
                
                if page != 1:
                    newURL += str(page) + '.htm'
                self.root.get(newURL)

                # 解析出 区，地址，邮编
                trs = self.root.find_elements_by_xpath('/html/body/table[7]/tbody/tr')
                for tr in trs[1:]:
                    num += 1
                    res = etree.HTML(tr.get_attribute('outerHTML'))
                    area = res.xpath('//tr/td[1]/text()')[0]
                    address = res.xpath('//tr/td[2]/text()')[0]
                    code = res.xpath('//tr/td[3]/text()')[0]

                    # 存储数据 
                    AllCodeSql().insertData(num, code, province, city, area, address)
        self.root.quit()



if __name__ == "__main__":
    # 创建数据库->创建数据表
    # AllCodeSql().createAllCodeTable()

    # 
    # Pcode().provinceUrl()
    # Pcode().provinceToCity()
    # Pcode().allCity()
    # Pcode().saveCode()

