import sqlite3

class AllCodeSql(object):
    '''所有邮编'''
    con = sqlite3.connect('allCode.db')
    cur = con.cursor()

    def createAllCodeTable(self):
        sql = 'create table posyCodeAddress(id INTEGER PRIMARY KEY AUTOINCREMENT, code varchar(10), province varchar(10), city varchar(20), area varchar(20), address varchar(50));'
        self.cur.execute(sql)
        self.con.commit()
        self.con.close()
        print('创建数据库 allCode.db -- posiCodeAddress 成功')

     def insertData(self, id, code, province, city, area, address):
        sql = f'insert into postCodeAddress(id,code,province,city,area,address) values({id},"{code}","{province}","{city}","{area}","{address}");'
        self.cur.execute(sql)
        self.con.commit()
        print(f'{id}--{code}--{province}--{city}--{area}--{address}  添加成功')


class CityCodeSql(object):
    '''市级邮编'''
    con = sqlite3.connect('cityCode.db')
    cur = con.cursor()

    def createCityCodeTable(self):
        sql = 'create table cityCode(id int,code varchar(10),province varchar(20),city varchar(50));'
        self.cur.execute(sql)
        self.con.commit()
        self.con.close()
        print('创建数据库 cityCode.db -- cityCode 成功')
    
    def insertDada(self, num, code, province, city):
        sql = f'insert into cityCode(id, code, province, city) values({int(num)},"{code}","{province}","{city}");'
        self.cur.execute(sql)
        self.con.commit()
        print(f'{num}--{code}--{province}--{city}  添加成功')


class AreaCodeSql(object):
    '''县级邮编'''
    con = sqlite3.connect('areaCode.db')
    cur = con.cursor()

    def createAreaCodeTable(self):
        sql = 'create table areaCode(id int,code varchar(10),province varchar(20),city varchar(50),area varchar(50));'
        self.cur.execute(sql)
        self.con.commit()
        self.con.close()
        print('创建数据库 areaCode.db -- areaCode 成功')
    
    def insertDada(self, num, code, province, city, area):
        sql = f'insert into areaCode(id,code,province,city,area) values({int(num)},"{code}","{province}","{city}","{area}");'
        self.cur.execute(sql)
        self.con.commit()
        print(f'{num}--{code}--{province}--{city}--{area}  添加成功')
