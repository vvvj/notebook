from jsonFile import JsonFile
from sql import CityCodeSql
from selenium import webdriver
from lxml import etree

class CityCode(object):
    '''市级邮编'''
    root = webdriver.Chrome()

    oneProvinceUrl = []
    oneCityUrl = []
    num = 0

    def provinceUrl(self):
        '''除去港澳台，获取各省及直辖市URL，分别存储在oneProvinceUrl，oneCityUrl 两个list中'''
        url = 'http://alexa.ip138.com/post/jiangxi/jingdezhen/'
        self.root.get(url)
        url_list = self.root.find_elements_by_xpath('/html/body/table[4]/tbody/tr[1]/td/a')
        for i in url_list[:-3]:
            aValue = i.text
            if aValue[-1:] == '市':
                self.oneCityUrl.append(i.get_attribute('href'))
            else:
                self.oneProvinceUrl.append(i.get_attribute('href'))
        print(self.oneProvinceUrl)
        print('---------')
        print(self.oneCityUrl)


    def saveCityCode(self):
        '''保存直辖市的邮编'''
        for i in self.oneCityUrl:
            self.root.get(i)
            self.root.find_element_by_xpath('/html/body/table[4]/tbody/tr[2]/td[1]/a/b').click()
            self.root.find_element_by_xpath('/html/body/table[4]/tbody/tr[2]/td/a[1]').click()
            
            self.num += 1
            province = ''
            city = self.root.find_element_by_xpath('/html/body/table[6]/tbody/tr[3]/td[1]/a').text
            code = self.root.find_element_by_xpath('/html/body/table[6]/tbody/tr[3]/td[3]').text

            CityCodeSql().insertDada(self.num, code[3:], province, city)
            print(f'{self.num}--{city}--{code}')


    def saveProvinceCode(self):
        '''保存各省市的邮编'''
        for i in self.oneProvinceUrl:
            self.root.get(i)
            self.root.find_element_by_xpath('/html/body/table[4]/tbody/tr[2]/td[1]/a/b').click()
            citysElement = self.root.find_elements_by_xpath('/html/body/table[4]/tbody/tr[2]/td/a')
            citysUrl = [cityElement.get_attribute('href') for cityElement in citysElement]

            for cityUrl in citysUrl:
                self.root.get(cityUrl)

                self.num += 1
                province = self.root.find_element_by_xpath('/html/body/table[6]/tbody/tr[3]/td[1]/a').text
                city = self.root.find_element_by_xpath('/html/body/table[6]/tbody/tr[3]/td[2]/a').text
                code = self.root.find_element_by_xpath('/html/body/table[6]/tbody/tr[3]/td[3]').text

                CityCodeSql().insertDada(self.num, code[3:], province, city)
                print(f'{self.num}--{province}--{city}--{code}')




if __name__ == "__main__":
    # 创建数据库
    # CityCodeSql().createCityCodeTable()
    CityCode().provinceUrl()
    CityCode().saveCityCode()
    CityCode().saveProvinceCode()


