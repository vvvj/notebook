import json

class JsonFile(object):
    '''操作json文件'''
    
    def jsonWriteFile(self, filename, data):
        '''写入json文件'''
        with open(filename, 'w')as f:
            f.write(json.dumps(data, indent=2, ensure_ascii=False))
            print(f'{filename}存储成功')

    def jsonReadFile(self, filename):
        '''读取json文件'''
        with open(filename, 'r')as f:
            data = json.load(f)
            return data