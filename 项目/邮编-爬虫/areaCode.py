from jsonFile import JsonFile
from sql import AreaCodeSql
from selenium import webdriver
from selenium.common import exceptions as ex
from lxml import etree

class AreaCode(object):
    '''县级邮编'''
    root = webdriver.Chrome()
    root.implicitly_wait(10)   # 隐性等待10s

    oneProvinceUrl = []
    oneCityUrl = []
    num = 0

    def provinceUrl(self):
        '''除去港澳台，获取各省及直辖市URL，分别存储在oneProvinceUrl，oneCityUrl 两个list中'''
        url = 'http://alexa.ip138.com/post/jiangxi/jingdezhen/'
        self.root.get(url)
        url_list = self.root.find_elements_by_xpath('/html/body/table[4]/tbody/tr[1]/td/a')
        for i in url_list[:-3]:
            aValue = i.text
            if aValue[-1:] == '市':
                self.oneCityUrl.append(i.get_attribute('href'))
            else:
                self.oneProvinceUrl.append(i.get_attribute('href'))
        print(self.oneProvinceUrl)
        print('---------')
        print(self.oneCityUrl)


    def saveCityToAreaCode(self):
        '''保存直辖市的邮编'''
        for i in self.oneCityUrl:
            self.root.get(i)
            self.root.find_element_by_xpath('/html/body/table[4]/tbody/tr[2]/td[1]/a/b').click()

            lists = self.root.find_elements_by_xpath('/html/body/table[4]/tbody/tr[2]/td/a')
            for j in range(len(lists)):
                self.root.find_elements_by_xpath('/html/body/table[4]/tbody/tr[2]/td/a')[j].click()
                areaList = self.root.find_elements_by_xpath('/html/body/table[4]/tbody/tr[3]/td/a')
                
                for k in range(len(areaList)):
                    self.root.find_elements_by_xpath('/html/body/table[4]/tbody/tr[3]/td/a')[k].click()
                    
                    self.num += 1
                    province = ''
                    city = self.root.find_element_by_xpath('/html/body/table[6]/tbody/tr[3]/td[1]/a').text
                    area = self.root.find_element_by_xpath('/html/body/table[7]/tbody/tr[2]/td[1]').text
                    code = self.root.find_element_by_xpath('/html/body/table[6]/tbody/tr[3]/td[3]').text

                    AreaCodeSql().insertDada(self.num, code[3:], province, city, area[2:])


    def saveProvinceToAreaCode(self):
        '''保存各省市的邮编'''
        for i in self.oneProvinceUrl:
            self.root.get(i)
            self.root.find_element_by_xpath('/html/body/table[4]/tbody/tr[2]/td[1]/a/b').click()

            lists = self.root.find_elements_by_xpath('/html/body/table[4]/tbody/tr[2]/td/a')
            for j in range(len(lists)):
                self.root.find_elements_by_xpath('/html/body/table[4]/tbody/tr[2]/td/a')[j].click()
                areaList = self.root.find_elements_by_xpath('/html/body/table[4]/tbody/tr[3]/td/a')

                for k in range(len(areaList)):
                    self.root.find_elements_by_xpath('/html/body/table[4]/tbody/tr[3]/td/a')[k].click()
                    

                    self.num += 1
                    province = self.root.find_element_by_xpath('/html/body/table[6]/tbody/tr[3]/td[1]/a').text
                    city = self.root.find_element_by_xpath('/html/body/table[6]/tbody/tr[3]/td[2]/a').text
                    area = self.root.find_element_by_xpath('/html/body/table[7]/tbody/tr[2]/td[1]').text
                    code = self.root.find_element_by_xpath('/html/body/table[6]/tbody/tr[3]/td[3]').text

                    AreaCodeSql().insertDada(self.num, code[3:], province, city, area[len(city):])






if __name__ == "__main__":
    # 创建数据库
    AreaCodeSql().createAreaCodeTable()
    AreaCode().provinceUrl()
    AreaCode().saveCityToAreaCode()
    AreaCode().saveProvinceToAreaCode()

