import requests
from bs4 import BeautifulSoup
from time import sleep
import json

# 糗事百科-文字
class Qiushibaike_Wenzi(object):
	def __init__(self):
		self.qiubai_wenzi_over()
		print('over')

	# 爬取所有的url 
	def qiubai_wenzi_page_url(self):
		lists=[]
		for i in range(1,14):
			url = 'https://www.qiushibaike.com/text/page/%d'%i
			res = requests.get(url)

			soup = BeautifulSoup(res.text, 'lxml')
			urls = soup.find_all('a', class_='contentHerf')
			for url in urls:
				new_url = 'https://www.qiushibaike.com'+str(url['href'])
				lists.append(new_url)
				print(new_url)
			print('第%d页完成'%i)
			sleep(1)
		return lists


	# 传入url列表，获取每一个页面的数据存入列表
	def qiubai_wenzi_page_text(self, urls):
		lists=[]
		num=0
		for i in urls:
			res = requests.get(i)
			soup = BeautifulSoup(res.text, 'lxml')
			content = soup.find('div', class_='content')
			dicts={'content':content.text}
			lists.append(dicts)
			num += 1
			print('第%d个'%num)
		return lists

	# 传入数据列表，把数据写入json文件
	def qiubai_wenzi_over(self):
		urls = self.qiubai_wenzi_page_url()
		data = self.qiubai_wenzi_page_text(urls)
		with open('qiubai_text.json', 'w', encoding = 'utf-8') as f:
			f.write(json.dumps(data, indent = 2, ensure_ascii = False))
		print('ok')



# 糗事百科-新鲜
class Qiushibaike_Xinxian(object):
	def __init__(self):
		self.json_w()
		print('over')

	def json_w(self):
		data = { 'name':'zhangsan', 'age':12}
		with open('qiushibaike_xinxian.json', 'a', encoding = 'utf-8') as f:
			f.write(json.dumps(data, indent = 2, ensure_ascii = False))
		print('写-完成')

Qiushibaike_Xinxian()