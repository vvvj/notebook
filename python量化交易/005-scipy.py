import numpy as np 
import scipy.stats as stats
import scipy.optimize as opt 


# 生成随机数
num1 = stats.uniform.rvs(size = 10)          # 均匀分布
num2 = stats.beta.rvs(size = 10, a = 4, b = 2)  # 贝塔分布

print(num1)
print(num2)