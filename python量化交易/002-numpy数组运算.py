import numpy as np


# 数组相加
a1 = np.array([1, 2])
a2 = np.array([3, 4]) 
res1 = a1 + a2        # 一维整数数组相加 [4 6]

a3 = np.array([[1, 2], [3, 4]])
a4 = np.array([[3, 5], [2, 7]])
res2 = a3 + a4        # 二维整数数组相加 [[4 7] [5 11]]


a5 = np.array([1.4, 2])
a6 = np.array([3, 3.9]) 
res3 = a5 + a6        # 一维浮点数数组相加  [4.4  5.9]

# print(res1)
# print(res2)
# print(res3)






# 数组 加减乘除 数值
a = np.array([[1, 2.3], [5, 6]])
# print(a + 1.2)
# print(a - 0.5)
# print(a * 3)
# print(a / 2)







# 开平方
a = np.array([2, 4])
# print(np.exp(a))        # e 的幂次方  e常数：2.71828
# print(np.sqrt(a))       # 开平方
# print(np.square(a))     # 求平方
# print(np.power(a, 3))   # 求次方