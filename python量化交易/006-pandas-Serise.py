import numpy as np 
from pandas import Series



# 数组创建  Series
a = np.random.rand(5)
s1 = Series(a)
s2 = Series(a, index = ['a', 'b', 'c', 'd', 'e'])
s3 = Series(a, index = ['a', 'b', 'c', 'd', 'e'], name = 'test')

# print(s1)
# print(s2)
# print(s3)
# print(s3.name)





# 字典创建 Series
d = {'name': 'Huyang', 'age': 11}
s1 = Series(d)
s2 = Series(d, index = ['age', 'name', 'c'])
# print(s1)
# print(s2)





# Serise 数据访问
a = np.random.rand(10)
s = Series(a)

# print(s[0])
# print(s[:3])
# print(s[s > 0.5])