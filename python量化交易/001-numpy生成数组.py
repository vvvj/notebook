import numpy as np 

# 打印版本号
# print(np.version.full_version)



# 生成数组
a = np.arange(20)          # 生成一维数组
a1 = a.reshape(4, 5)       # 生成二维数组
a2 = a.reshape(2, 2, 5)    # 生成三维数组
# print(a)
# print(a1)
# print(a2)



# 查看数组属性，例a2
s1 = a2.ndim       # 查看维度
s2 = a2.shape      # 查看各维度的大小
s3 = a2.size       # 查看元素个数
s4 = a2.dtype      # 查看元素类型

# print(s1)
# print(s2)
# print(s3)
# print(s4)





# 列表转数组
list1 =  [1, 2, 3, 4, 5]
list2 = [[1, 3, 5], [2, 4, 6]]

a1 = np.array(list1)
a2 = np.array(list2)
# print(a1)
# print(a2)





# 生成特殊矩阵
a1 = np.zeros((4, 5))                 # 生成全零矩阵
a2 = np.ones((4, 5), dtype = int)     # 生成全一矩阵
a3 = np.random.rand(3)                # 生成 [0, 1] 区间的随机数数组
# print(a1)
# print(a2)
# print(a3)





