import numpy as np 
import numpy.linalg as nlg


# 矩阵转置
a1 = np.random.rand(2,4)
a2 = np.transpose(a1)       # 由a1  2*4 转置为 4*2
# print(a1)
# print(a2)


b1 = np.random.rand(2,4)
b2 = np.transpose(b1)       # 由b1  2*4 转置为 4*2
# print(b1)
# print(b2)








# 矩阵求逆
a = np.array([[1, 2], [3, 4]])   # 生成矩阵
ia = nlg.inv(a)                  # 求逆
# print(a)
# print(ia)







# 求特征值和特征向量
a = np.random.rand(3, 3)
res1, res2 = nlg.eig(a)
# print(res1)
# print(res2)








# 矩阵拼接
a1 = np.random.rand(2, 2)    # 生成 2*2 矩阵
a2 = np.random.rand(2, 2)    # 生成 2*2 矩阵

b1 = np.hstack([a1, a2])     # 生成 2*4 矩阵
b2 = np.vstack([a1, a2])     # 生成 4*2 矩阵

# print(a1)
# print(a2)
# print(b1)
# print(b2)