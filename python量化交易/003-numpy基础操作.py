import numpy as np 


# 求和、最大数、最小数
a = np.arange(20).reshape(4, 5)

res_sum = a.sum()    # 求和
res_max = a.max()    # 求最大数
res_min = a.min()    # 求最小数

res_yy = a.max(axis = 1)   # 返回每个维度（列）的最大数

# print(res_sum)
# print(res_max)
# print(res_min)
# print(res_yy)









# 数组元素访问、修改
a = np.array([[1, 2], [2, 3], [3, 4]])

res1 = a[1][0]   # 访问
res2 = a[1, 0]   # 访问

a[1][0] = 8      # 修改

print(res1)
print(res2)
print(a)